﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MaterialTracker.ViewModels;
using Npgsql;

namespace MaterialTracker.Controllers
{
    public class NotesController : Controller
    {
        public bool GetUserInfo()
        {
            string ADLogin = User.Identity.Name.Replace("STONE\\", "");

            using (var conn = new NpgsqlConnection(ExtensionMethod.ConnectionString_ERPSVR1))
            {
                //open the connection to the database
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    //parse through the table based on the query string and add it into variable in class
                    cmd.Connection = conn;
                    cmd.CommandText = @"select likestobecalled || ' ' || LastName, employeeid from hr.tblhr_employees where active = '1' and ADLoginID like @ADLogin";
                    cmd.Parameters.AddWithValue("@ADLogin", ADLogin);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ViewBag.UserName = reader.IsDBNull(0) ? "" : reader.GetString(0);
                            ViewBag.UserID = reader.IsDBNull(1) ? "" : reader.GetInt32(1).ToString();
                        }
                    }
                }
            }

            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"select * from STONE_DB.dbo.permission_users where employee_id = @EmpID and permission_type_id = 44";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@EmpID", Convert.ToInt32(ViewBag.UserID));
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        return true;
                    }
                }
                cmd.Connection.Close();
            }

            return false;
        }

        // GET: Notes
        public ActionResult NotesIndex()
        {
            GetUserInfo();

            ViewBag.tblid = Request.QueryString["tblid"] == null ? 0 : Convert.ToInt32(Request.QueryString["tblid"]);
            ViewBag.fmt = Request.QueryString["fmt"] == null ? 0 : Convert.ToInt32(Request.QueryString["fmt"]);
            ViewBag.NoteID = Request.QueryString["noteid"] == null ? 0 : Convert.ToInt32(Request.QueryString["noteid"]);
            ViewBag.SearchString = Request.QueryString["searchstring"] == null ? "" : Request.QueryString["searchstring"].ToString();


            NotesEdit myNotes = new NotesEdit
            {
                MaterialID = Convert.ToInt32(ViewBag.tblid)
            };
            myNotes.LoadNotes();

            if (ViewBag.NoteID != 0)
            {
                myNotes.NewNote = myNotes.ReturnEditNote(ViewBag.NoteID);
            }
            return View(myNotes);
        }

        [HttpPost]
        public ActionResult NotesIndex(NotesEdit model, string submit)
        {
            GetUserInfo();
            ViewBag.tblid = Request.QueryString["tblid"] == null ? 0 : Convert.ToInt32(Request.QueryString["tblid"]);
            ViewBag.fmt = Request.QueryString["fmt"] == null ? 0 : Convert.ToInt32(Request.QueryString["fmt"]);
            ViewBag.NoteID = Request.QueryString["noteid"] == null ? 0 : Convert.ToInt32(Request.QueryString["noteid"]);

            NotesEdit myNotes = new NotesEdit
            {
                MaterialID = Convert.ToInt32(ViewBag.tblid),
                NewNote = model.NewNote
            };

            if(ModelState.IsValid)
            {
                if(ViewBag.NoteID == 0)
                {
                    myNotes.AddNote(Convert.ToInt32(ViewBag.UserID));
                }
                else
                {
                    myNotes.UpdateNote(ViewBag.NoteID, Convert.ToInt32(ViewBag.UserID));
                }

                return RedirectToAction("NotesIndex", "Notes", new { ViewBag.tblid, ViewBag.fmt });
            }
            myNotes.LoadNotes();
            return View(model);
        }

        public ActionResult DeleteNote()
        {
            GetUserInfo();
            ViewBag.tblid = Request.QueryString["tblid"] == null ? 0 : Convert.ToInt32(Request.QueryString["tblid"]);
            ViewBag.fmt = Request.QueryString["fmt"] == null ? 0 : Convert.ToInt32(Request.QueryString["fmt"]);
            ViewBag.NoteID = Request.QueryString["NoteID"] == null ? 0 : Convert.ToInt32(Request.QueryString["NoteID"]);

            if(Convert.ToInt32(ViewBag.NoteID) > 0)
            {
                //DELETE IT HERE
                NotesEdit myNotes = new NotesEdit();

                myNotes.DeleteNote(Convert.ToInt32(ViewBag.NoteID));
            }

            return RedirectToAction("NotesIndex", "Notes", new { ViewBag.tblid, ViewBag.fmt });
        }

        public ActionResult EditNote()
        {
            GetUserInfo();
            ViewBag.tblid = Request.QueryString["tblid"] == null ? 0 : Convert.ToInt32(Request.QueryString["tblid"]);
            ViewBag.fmt = Request.QueryString["fmt"] == null ? 0 : Convert.ToInt32(Request.QueryString["fmt"]);
            ViewBag.NoteID = Request.QueryString["noteid"] == null ? 0 : Convert.ToInt32(Request.QueryString["noteid"]);


            return RedirectToAction("NotesIndex", "Notes", new { ViewBag.tblid, ViewBag.fmt, noteid = ViewBag.NoteID });
        }
    }
}