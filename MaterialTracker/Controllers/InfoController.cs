﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MaterialTracker.ViewModels;
using Npgsql;
using System.Data.SqlClient;

namespace MaterialTracker.Controllers
{
    public class InfoController : Controller
    {
        public bool GetUserInfo()
        {
            string ADLogin = User.Identity.Name.Replace("STONE\\", "");

            using (var conn = new NpgsqlConnection(ExtensionMethod.ConnectionString_ERPSVR1))
            {
                //open the connection to the database
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    //parse through the table based on the query string and add it into variable in class
                    cmd.Connection = conn;
                    cmd.CommandText = @"select likestobecalled || ' ' || LastName, employeeid from hr.tblhr_employees where active = '1' and ADLoginID like @ADLogin";
                    cmd.Parameters.AddWithValue("@ADLogin", ADLogin);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ViewBag.UserName = reader.IsDBNull(0) ? "" : reader.GetString(0);
                            ViewBag.UserID = reader.IsDBNull(1) ? "" : reader.GetInt32(1).ToString();
                        }
                    }
                }
            }

            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"select * from STONE_DB.dbo.permission_users where employee_id = @EmpID and permission_type_id = 44";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@EmpID", Convert.ToInt32(ViewBag.UserID));
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        return true;
                    }
                }
                cmd.Connection.Close();
            }

            return false;
        }

        // GET: Info
        public ActionResult InfoIndex()
        {
            GetUserInfo();
            Info myInfo = new Info();

            ViewBag.tblid = Request.QueryString["tblid"] == null ? 0 : Convert.ToInt32(Request.QueryString["tblid"]);
            ViewBag.fmt = Request.QueryString["fmt"] == null ? 0 : Convert.ToInt32(Request.QueryString["fmt"]);
            myInfo.PageType = Request.QueryString["info"] == null ? 0 : Convert.ToInt32(Request.QueryString["info"]);
            ViewBag.SearchString = Request.QueryString["searchstring"] == null ? "" : Request.QueryString["searchstring"].ToString();

            myInfo.GetItemID(Convert.ToInt32(ViewBag.tblid));

            if (myInfo.PageType == 1)
            {
                myInfo.LoadLocations();
            }
            else if (myInfo.PageType == 2)
            {
                myInfo.LoadUsage(Convert.ToInt32(ViewBag.tblid));
            }
            else if(myInfo.PageType == 3)
            {
                myInfo.LoadWhereUsed();
            }

            return View(myInfo);
        }
    }
}