﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MaterialTracker.ViewModels;
using Npgsql;

namespace MaterialTracker.Controllers
{
    public class EditItemController : Controller
    {
        public bool GetUserInfo()
        {
            string ADLogin = User.Identity.Name.Replace("STONE\\", "");

            using (var conn = new NpgsqlConnection(ExtensionMethod.ConnectionString_ERPSVR1))
            {
                //open the connection to the database
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    //parse through the table based on the query string and add it into variable in class
                    cmd.Connection = conn;
                    cmd.CommandText = @"select likestobecalled || ' ' || LastName, employeeid from hr.tblhr_employees where active = '1' and ADLoginID like @ADLogin";
                    cmd.Parameters.AddWithValue("@ADLogin", ADLogin);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ViewBag.UserName = reader.IsDBNull(0) ? "" : reader.GetString(0);
                            ViewBag.UserID = reader.IsDBNull(1) ? "" : reader.GetInt32(1).ToString();
                        }
                    }
                }
            }

            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"select * from STONE_DB.dbo.permission_users where employee_id = @EmpID and permission_type_id = 36";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@EmpID", Convert.ToInt32(ViewBag.UserID));
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        return true;
                    }
                }
                cmd.Connection.Close();
            }

            return false;
        }

        // GET: AddItem
        public ActionResult Index()
        {
            ViewBag.EditPermission = GetUserInfo();
            EditMaterial newNode = new EditMaterial();
            MaterialsNode newNode2 = new MaterialsNode();

            ViewBag.tblid = Request.QueryString["tblid"] == null ? 0 : Convert.ToInt32(Request.QueryString["tblid"]);
            ViewBag.fmt = Request.QueryString["fmt"] == null ? 0 : Convert.ToInt32(Request.QueryString["fmt"]);
            ViewBag.SearchString = Request.QueryString["searchstring"] == null ? "" : Request.QueryString["searchstring"].ToString();

            ViewData["Item"] = newNode.myItems;
            ViewData["Vendor"] = newNode.myVendors;

            newNode2.LoadSingleItem(ViewBag.tblid, Convert.ToInt32(ViewBag.UserID));
            ViewBag.Title = "Edit " + newNode2.ItemNumber;
            ViewBag.SelectedItem = newNode2.ItemId;
            ViewBag.SelectedVendor = newNode2.VendorID;
            ViewBag.History = newNode2.History.ToString();

            return View(newNode2);
        }

        [HttpPost]
        public ActionResult Index(MaterialsNode model)
        {
            ViewBag.EditPermission = GetUserInfo();
            ModelState.Remove("mininv");
            ModelState.Remove("OrderFrequency");
            ModelState.Remove("MaxInventoryValue");
            ViewBag.tblid = Request.QueryString["tblid"] == null ? 0 : Convert.ToInt32(Request.QueryString["tblid"]);
            ViewBag.fmt = Request.QueryString["fmt"] == null ? 0 : Convert.ToInt32(Request.QueryString["fmt"]);
            ViewBag.SearchString = Request.QueryString["searchstring"] == null ? "" : Request.QueryString["searchstring"].ToString();

            if (ModelState.IsValid)
            {
                //Updates the usage so when the user is sent back to the page it is updated with new values
                model.GetWeeklyUsage();

                //Do save to the material tracker here
                using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
                {
                    string sql = @"update material_trackers set release_qty = @Release, order_frequency = @Order, vend_id = @Vend, lead_time = @Lead, updated_at = @Now, min_inv = @MinInv,
                                history_weight = @HisWeight, forecast_weight = @ForWeight, forecast = @Forecast, history = @History, low_usage = @LowUsage, new_item = @NewItem, user_watch = @UserWatch, 
                                max_inventory_value = @MaxValue where Id = @MatTrackID";
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Connection.Open();
                    cmd.Parameters.AddWithValue("@MatTrackID", Convert.ToInt32(ViewBag.tblid));
                    cmd.Parameters.AddWithValue("@Release", model.ReleaseQty);
                    cmd.Parameters.AddWithValue("@Order", model.OrderFrequency);
                    cmd.Parameters.AddWithValue("@Vend", model.VendorID);
                    cmd.Parameters.AddWithValue("@Lead", model.LeadTime);
                    cmd.Parameters.AddWithValue("@MinInv", model.MinInv);
                    cmd.Parameters.AddWithValue("@HisWeight", model.HistoryWeight);
                    cmd.Parameters.AddWithValue("@ForWeight", model.ForecastWeight);
                    cmd.Parameters.AddWithValue("@Forecast", model.Forecast);
                    cmd.Parameters.AddWithValue("@History", model.History);
                    cmd.Parameters.AddWithValue("@Now", DateTime.Now.AddDays(-1));
                    cmd.Parameters.AddWithValue("@LowUsage", DateTime.Now.AddDays(-1));
                    cmd.Parameters.AddWithValue("@NewItem", false);
                    cmd.Parameters.AddWithValue("@UserWatch", model.UpdateUserWatchString(model.SelectUserWatch, Convert.ToInt32(ViewBag.UserID), Convert.ToInt32(ViewBag.tblid)));
                    cmd.Parameters.AddWithValue("@MaxValue", model.MaxInventoryValue);
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();
                }

                if (ViewBag.SearchString.ToString().Equals(""))
                {
                    return RedirectToAction("Index", "Home", new { ViewBag.fmt });
                }
                else
                {
                    return RedirectToAction("Index", "Home", new { ViewBag.SearchString });
                }
            }
            else
            {
                EditMaterial newNode = new EditMaterial();
                ViewData["Item"] = newNode.myItems;
                ViewData["Vendor"] = newNode.myVendors;
                ViewBag.Title = "Edit " + model.ItemNumber;
                ViewBag.SelectedItem = model.ItemId;
                ViewBag.SelectedVendor = model.VendorID;
                ViewBag.History = model.History.ToString();

                return View(model);
            }
        }
    }
}