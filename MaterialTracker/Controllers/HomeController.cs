﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using Npgsql;
using MaterialTracker.ViewModels;

namespace MaterialTracker.Controllers
{
    public class HomeController : Controller
    {
        public MatTrackData myData = new MatTrackData();

        public bool GetUserInfo()
        {
            string ADLogin = User.Identity.Name.Replace("STONE\\", "");

            using (var conn = new NpgsqlConnection(ExtensionMethod.ConnectionString_ERPSVR1))
            {
                //open the connection to the database
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    //parse through the table based on the query string and add it into variable in class
                    cmd.Connection = conn;
                    cmd.CommandText = @"select likestobecalled || ' ' || LastName, employeeid from hr.tblhr_employees where active = '1' and ADLoginID like @ADLogin";
                    cmd.Parameters.AddWithValue("@ADLogin", ADLogin);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ViewBag.UserName = reader.IsDBNull(0) ? "" : reader.GetString(0);
                            ViewBag.UserID = reader.IsDBNull(1) ? "" : reader.GetInt32(1).ToString();
                        }
                    }
                }
            }

            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"select * from STONE_DB.dbo.permission_users where employee_id = @EmpID and permission_type_id = 36";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@EmpID", Convert.ToInt32(ViewBag.UserID));
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        return true;
                    }
                }
                cmd.Connection.Close();
            }

            return false;
        }

        public void LoadSearchTerms()
        {
            using (var conn = new NpgsqlConnection(ExtensionMethod.ConnectionString_ERPSVR1))
            {
                //open the connection to the database
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT item_number FROM item where item_active and (item_number ilike 'COM%' or item_number ilike 'COL%' or item_number ilike 'RES%' or item_number ilike 'PKG%') order by item_number";

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ViewBag.HiddenSearchTags += reader.GetString(0) + ",";
                        }
                    }
                }
            }
        }

        public void SetupTogglesAndLoadMaterials(string SearchString)
        {
            int MyWatch = 0;
            int Status = 0;

            if (ViewBag.QueryString >= 100 && ViewBag.QueryString != 1000)
            {
                int TmpStatus = ViewBag.QueryString - 100;
                if (TmpStatus / 90 == 1)
                {
                    Status = 90;
                }
                else if (TmpStatus / 50 == 1)
                {
                    Status = 50;
                }
                else if (TmpStatus / 40 == 1)
                {
                    Status = 40;
                }
                else if (TmpStatus / 30 == 1)
                {
                    Status = 30;
                }
                else if (TmpStatus / 20 == 1)
                {
                    Status = 20;
                }
                else if (TmpStatus / 10 == 1)
                {
                    Status = 10;
                }

                MyWatch = 100;
            }
            else
            {
                if (ViewBag.QueryString / 90 == 1)
                {
                    Status = 90;
                }
                else if (ViewBag.QueryString / 50 == 1)
                {
                    Status = 50;
                }
                else if (ViewBag.QueryString / 40 == 1)
                {
                    Status = 40;
                }
                else if (ViewBag.QueryString / 30 == 1)
                {
                    Status = 30;
                }
                else if (ViewBag.QueryString / 20 == 1)
                {
                    Status = 20;
                }
                else if (ViewBag.QueryString / 10 == 1)
                {
                    Status = 10;
                }
            }

            ViewBag.Status = Status;
            ViewBag.Type = ViewBag.QueryString - Status - MyWatch;
            ViewBag.MyWatch = MyWatch;

            myData.LoadMaterials(ViewBag.Status, ViewBag.Type, ViewBag.MyWatch, Convert.ToInt32(ViewBag.UserID), SearchString);
        }

        public ActionResult Index()
        {
            ViewBag.SearchString = Request.QueryString["searchstring"] == null ? "" : Convert.ToString(Request.QueryString["searchstring"]);
            LoadSearchTerms();

            //Get Current User for permissions
            ViewBag.EditPermission = GetUserInfo();
            //ViewBag.ButtonBarSize = Convert.ToBoolean(ViewBag.EditPermission) ? "70%" : "55%";

            //Set Page Title
            ViewBag.PageTitle = "Material Tracker";

            //Load what format the 
            ViewBag.QueryString = Request.QueryString["fmt"] == null ? 0 : Convert.ToInt32(Request.QueryString["fmt"]);

            SetupTogglesAndLoadMaterials(ViewBag.SearchString);

            return View(myData);
        }

        public ActionResult Search(MatTrackData model)
        {
            return RedirectToAction("Index", new { searchstring = model.MatSearchString });
        }
    }
}