﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace MaterialTracker.ViewModels
{
    public static class ExtensionMethod
    {
        public static string ConnectionString_ERPSVR1 = "Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP";
        public static string Get2016ServerConnectionString()
        {
            string HostName = System.Net.Dns.GetHostName();
            if (HostName.ToLower().Equals("spapps"))
            {
                return "Data Source=SPMSSQL16Prod;Initial Catalog=SharePointData;Persist Security Info=True;User ID=readwrite;Password=246stone!";
            }
            else
            {
                return "Data Source=SPMSSQL16test;Initial Catalog=SharePointDataDev;Persist Security Info=True;User ID=admin;Password=stone246";
            }
        }

        public static int CreateHeader(int _PageID, string _NewHeaderName, int _MaxOrderNumber, int _UserID)
        {
            int ReturnValue = -1;

            using (SqlConnection conn = new SqlConnection(Get2016ServerConnectionString()))
            {
                string sql = @"DECLARE @tmpTable TABLE (InsertedID INT);
                                            INSERT INTO cs.dash_headers (
	                                            header_name
	                                            ,order_number
	                                            ,page_id
	                                            ,emp_created_by_id
	                                            ,emp_updated_by_id
	                                            )
                                            OUTPUT INSERTED.Id into @tmpTable
                                            VALUES (
	                                            @HeaderName
	                                            ,@OrderNumber
	                                            ,@PageID
	                                            ,@UserID
	                                            ,@UserID
	                                            )
                                            SELECT InsertedID
                                            FROM @tmpTable";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@PageID", _PageID);
                cmd.Parameters.AddWithValue("@HeaderName", _NewHeaderName);
                cmd.Parameters.AddWithValue("@OrderNumber", _MaxOrderNumber);
                cmd.Parameters.AddWithValue("@UserID", _UserID);
                cmd.Connection.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ReturnValue = reader.GetInt32(0);
                    }
                }
                cmd.Connection.Close();
            }

            return ReturnValue;
        }

        public static int CreateSubHeader(int _HeaderID, string _NewSubHeaderName, int _UserID, int _MaxOrderNumber = 1)
        {
            int ReturnValue = -1;

            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"DECLARE @tmpTable TABLE (InsertedID INT);
                                            INSERT INTO cs.dash_subheaders (
	                                            subheader_name
	                                            ,order_number
	                                            ,header_id
	                                            ,emp_created_by_id
	                                            ,emp_updated_by_id
	                                            )
                                            OUTPUT INSERTED.Id
                                            INTO @tmpTable
                                            VALUES (
	                                            @SubHeaderName
	                                            ,@OrderNumber
	                                            ,@HeaderID
	                                            ,@UserID
	                                            ,@UserID
	                                            )
                                            SELECT InsertedID
                                            FROM @tmpTable";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@HeaderID", _HeaderID);
                cmd.Parameters.AddWithValue("@SubHeaderName", _NewSubHeaderName);
                cmd.Parameters.AddWithValue("@OrderNumber", _MaxOrderNumber);
                cmd.Parameters.AddWithValue("@UserID", _UserID);
                cmd.Connection.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ReturnValue = reader.GetInt32(0);
                    }
                }
                cmd.Connection.Close();
            }

            return ReturnValue;
        }

        public static int CreateURL(string _URLTitle, string _URLDescrip, string _URLURL, int _SubHeaderID, int _UserID, int _OrderNumber = 1, int _URLType = 5)
        {
            int ReturnValue = -1;

            if (!_URLURL.ToLower().StartsWith("http://"))
            {
                _URLURL = "http://" + _URLURL;
            }

            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"DECLARE @tmpTable TABLE (InsertedID INT);
                                            INSERT INTO cs.dash_urls (
	                                            url_type_id
	                                            ,url_title
	                                            ,url_descrip1
	                                            ,url_url
	                                            ,url_sub_header_id
	                                            ,url_order_number
	                                            ,emp_created_by_id
	                                            ,emp_updated_by_id
	                                            )
                                            OUTPUT INSERTED.url_id into @tmpTable
                                            VALUES (
	                                            @UrlTypeID
	                                            ,@UrlTitle
	                                            ,@UrlDescrip
	                                            ,@UrlUrl
	                                            ,@UrlSubHeaderID
	                                            ,@UrlOrderNumber
	                                            ,@UserID
	                                            ,@UserID
	                                            )
                                            SELECT InsertedID
                                            FROM @tmpTable";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@UrlTypeID", 4);
                cmd.Parameters.AddWithValue("@UrlTitle", _URLTitle);
                cmd.Parameters.AddWithValue("@UrlDescrip", _URLDescrip);
                cmd.Parameters.AddWithValue("@UrlUrl", _URLURL);
                cmd.Parameters.AddWithValue("@UrlSubHeaderID", _SubHeaderID);
                cmd.Parameters.AddWithValue("@UrlOrderNumber", _OrderNumber);
                cmd.Parameters.AddWithValue("@UserID", _UserID);
                cmd.Connection.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ReturnValue = reader.GetInt32(0);
                    }
                }
                cmd.Connection.Close();
            }

            return ReturnValue;
        }
    }

    public static class MaterialTrackerStatic
    {
        public static string MT_Site = "http://" + System.Net.Dns.GetHostName() + "/Workflows/MaterialTracking/";
        public static string MT_SiteMatID = "/Workflows/MaterialTracking/SitePages/Home.aspx?MatID=";
        public static string MT_New = "~/Workflows/MaterialTracking/Lists/MaterialTrackingList/NewForm.aspx?Source=http%3A%2F%2F" + System.Net.Dns.GetHostName() + "%2FWorkflows%2FMaterialTracking%2FLists%2FMaterialTrackingList%2FAllItems%2Easpx&RootFolder=";

        //Flags
        public static int MTL_Flag_Normal = 10;
        public static int MTL_Flag_Low = 20;
        public static int MTL_Flag_Danger = 30;
        public static int MTL_Flag_NoUsage = 40;
        public static int MTL_Flag_NewItem = 90;
    }

    public static class StaticVariables
    {
        public const int NumberofPOs = 8;
        public const int NumColumns = 8;

        public static int NoGroup = 0;
        public static int MaterialsGroup = 1;
        public static int LaunchGroup = 2;
    };

    public class MaterialDates
    {
        public int Number { get; set; }
        public DateTime OrderBy { get; set; }
        public DateTime Estimated { get; set; }
        public PurchaseOrder PO = new PurchaseOrder();
        public bool RunOutOfMaterial { get; set; }
        public bool POLate { get; set; }
        public bool NeedNewPO { get; set; }

        public MaterialDates()
        {
            Number = 0;
            OrderBy = DateTime.MinValue;
            Estimated = DateTime.MinValue;
            RunOutOfMaterial = false;
            POLate = false;
            NeedNewPO = false;
        }

    }

    public class PurchaseOrder
    {
        public DateTime Date { get; set; }
        public string OrderNumber { get; set; }
        public string EstMaterialLeft { get; set; }
        public double EstInventoryValue { get; set; }
        public bool PartBlanketPO { get; set; }

        public PurchaseOrder()
        {
            Date = DateTime.MinValue;
            OrderNumber = "";
            EstMaterialLeft = "";
            EstInventoryValue = 0;
            PartBlanketPO = false;
        }

        public void CheckForBlanketPO(string _PONumber, int _ItemID)
        {
            using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP"))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT  count(pohead_id)
	                                            FROM item
	                                            INNER JOIN itemsite ON itemsite_item_id = item_id
	                                            INNER JOIN poitem ON poitem_itemsite_id = itemsite_id
	                                            INNER JOIN pohead ON pohead_id = poitem_pohead_id
	                                            WHERE item.item_id = @RawMatID and pohead_number ilike @PONumber";
                    cmd.Parameters.AddWithValue("@RawMatID", _ItemID);
                    cmd.Parameters.AddWithValue("@PONumber", _PONumber);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.GetInt32(0) > 1)
                            {
                                PartBlanketPO = true;
                            }
                        }
                    }
                }
                conn.Close();
            }
        }
    }

    [Serializable]
    public class EditMaterial
    {
        public MaterialsNode myMat = new MaterialsNode();

        public List<SelectListItem> myItems = new List<SelectListItem>();
        public List<SelectListItem> myVendors = new List<SelectListItem>();

        public EditMaterial()
        {
            LoadItemsForList();
            LoadVendorsForList();
        }

        public void LoadItemsForList()
        {
            using (var conn = new NpgsqlConnection(ExtensionMethod.ConnectionString_ERPSVR1))
            {
                //open the connection to the database
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    //parse through the table based on the query string and add it into variable in class
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT item_id
	                                            ,item_number
                                            FROM item
                                            WHERE (item_number ilike 'RES%' OR item_number ilike 'COL%' OR item_number ilike 'PKG%' OR item_number ilike 'COM%') AND item_active = '1'
                                            ORDER BY item_number";
                    using (var reader = cmd.ExecuteReader())
                    {
                        SelectListItem newItem = new SelectListItem
                        {
                            Text = "Select Item",
                            Value = "0"
                        };
                        myItems.Add(newItem);
                        while (reader.Read())
                        {
                            newItem = new SelectListItem
                            {
                                Text = reader.IsDBNull(1) ? "" : reader.GetString(1),
                                Value = reader.IsDBNull(0) ? "-1" : reader.GetInt32(0).ToString()
                            };
                            myItems.Add(newItem);
                        }
                    }
                }
            }
        }

        public void LoadVendorsForList()
        {
            using (var conn = new NpgsqlConnection(ExtensionMethod.ConnectionString_ERPSVR1))
            {
                //open the connection to the database
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    //parse through the table based on the query string and add it into variable in class
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT vend_id
	                                            ,vend_name
                                            FROM vendinfo
                                            WHERE vend_active AND vend_vendtype_id IN (20, 21, 23, 24)
                                            ORDER BY vend_name";
                    using (var reader = cmd.ExecuteReader())
                    {
                        SelectListItem newItem = new SelectListItem
                        {
                            Text = "Select Vendor",
                            Value = "0"
                        };
                        myVendors.Add(newItem);
                        while (reader.Read())
                        {
                            newItem = new SelectListItem
                            {
                                Text = reader.IsDBNull(1) ? "" : reader.GetString(1),
                                Value = reader.IsDBNull(0) ? "-1" : reader.GetInt32(0).ToString()
                            };
                            myVendors.Add(newItem);
                        }
                    }
                }
            }
        }
    }

    [Serializable]
    public class NotesEdit
    {
        public int MaterialID { get; set; }
        [Required(ErrorMessage = "Please Enter in a Note")]
        public string NewNote { get; set; }
        public List<MatTrackNote> MaterialNotes = new List<MatTrackNote>();

        public string ReturnEditNote(int _EditNoteID)
        {
            foreach (MatTrackNote note in MaterialNotes)
            {
                if (_EditNoteID == note.NoteID)
                {
                    return note.note;
                }
            }

            return "";
        }

        public NotesEdit()
        {
            MaterialID = -1;
            NewNote = "";
        }

        public void LoadNotes()
        {
            int Count = 1;

            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"SELECT mat_note, Id from material_tracker_notes where mat_tracker_id = @MatID";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@MatID", MaterialID);
                cmd.Connection.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        MatTrackNote newNote = new MatTrackNote
                        {
                            note = reader.IsDBNull(0) ? "" : reader.GetString(0),
                            NoteID = reader.GetInt32(1),
                            Index = Count
                        };
                        MaterialNotes.Add(newNote);

                        Count++;
                    }
                }
                cmd.Connection.Close();
            }
        }

        public void AddNote(int _EmpID)
        {
            int newID = 0;

            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"INSERT INTO material_tracker_notes (mat_tracker_id, mat_note, created_by_emp_id, updated_by_emp_id) OUTPUT INSERTED.Id VALUES (@MatID, @Note, @EmpID, @EmpID)";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@MatID", MaterialID);
                cmd.Parameters.AddWithValue("@Note", NewNote);
                cmd.Parameters.AddWithValue("@EmpID", _EmpID);
                cmd.Connection.Open();
                newID = (Int32)cmd.ExecuteScalar();
                cmd.Connection.Close();
            }

            //Add it to our list
            if (newID != 0)
            {
                MatTrackNote newNote = new MatTrackNote
                {
                    note = NewNote,
                    NoteID = newID
                };

                MaterialNotes.Add(newNote);
            }
        }

        public void UpdateNote(int _EditNoteID, int _EmpID)
        {
            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"UPDATE material_tracker_notes SET mat_note = @Note, updated_by_emp_id = @EmpID where Id = @EditNoteID";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@Note", NewNote);
                cmd.Parameters.AddWithValue("@EditNoteID", _EditNoteID);
                cmd.Parameters.AddWithValue("@EmpID", _EmpID);
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
        }

        public void DeleteNote(int NoteID)
        {
            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"DELETE FROM material_tracker_notes WHERE Id = @NoteID";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@NoteID", NoteID);
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
        }
    }

    [Serializable]
    public class MatTrackNote
    {
        public string note { get; set; }
        public int NoteID { get; set; }
        public int Index { get; set; }

        public MatTrackNote()
        {
            NoteID = -1;
            note = "";
            Index = 0;
        }
    }

    [Serializable]
    public class MaterialsNode
    {
        public int ListItemID { get; set; }
        public string WarningMessage { get; set; }

        //Item information
        [Required(ErrorMessage = "Please Select an Item")]
        public int ItemId { get; set; }

        [Required(ErrorMessage = "Please Select a Vendor")]
        public int VendorID { get; set; }

        [Required(ErrorMessage = "Please Enter in the Release Quantity")]
        public int ReleaseQty { get; set; }

        [Required(ErrorMessage = "Please Enter in a Lead Time")]
        public int LeadTime { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "Please Select Either History or Forecast")]
        public bool UsageSelected { get; set; }

        public int MaxInventoryValue { get; set; }

        public string ItemNumber { get; set; }
        public string ItemDescription { get; set; }
        public string Vendor { get; set; }
        public int itemType { get; set; }

        //Material Tracker Information
        public int CalcMinInventory { get; set; }
        public bool[] Silo { get; set; }
        public DateTime LastUpdated { get; set; }
        public int ebobInventory { get; set; }
        public int WeeklyUsage { get; set; }
        public int Inventory { get; set; }
        public double WeeksLeft { get; set; }
        public bool HavePO { get; set; }
        public bool MissingPO { get; set; }
        public int MinInv { get; set; }
        public DateTime LowUsageDate { get; set; }
        public bool NewItem { get; set; }
        public bool SelectUserWatch { get; set; }
        public string UserWatch { get; set; }
        public int CurrentInventoryValue { get; set; }
        public int OrderFrequency { get; set; }
        public bool CalculatedMaxInventoryValue { get; set; }

        //History and/or Forecast
        public int History { get; set; }
        public bool Forecast { get; set; }
        public int HistoryWeight { get; set; }
        public int ForecastWeight { get; set; }
        public int DaysLookForward { get; set; }
        public int DaysLookPast { get; set; }
        public int MainHistoryValue { get; set; }
        public int MainForecastValue { get; set; }
        public string HoverInfo { get; set; }

        //Flag
        public int Flag { get; set; }
        public string FlagClass { get; set; }
        public bool LaunchRawItem { get; set; }

        public List<MaterialDates> myDates = new List<MaterialDates>();
        public List<MatTrackNote> myNotes = new List<MatTrackNote>();

        //Error List
        public string myError { get; set; }

        public MaterialsNode()
        {
            FlagClass = "panel-default";
            ItemNumber = "";
            ItemId = -1;
            Vendor = "";
            VendorID = -1;
            ReleaseQty = -1;
            LeadTime = -1;
            CalcMinInventory = -1;
            Silo = new bool[3];
            ItemDescription = "";
            UsageSelected = true;
            WeeklyUsage = 0;
            Inventory = -1;
            WeeksLeft = -1;
            ebobInventory = 0;
            HistoryWeight = 50;
            ForecastWeight = 50;
            Flag = -1;
            OrderFrequency = 0;
            HoverInfo = "";
            UserWatch = "";
            SelectUserWatch = false;
            MaxInventoryValue = 0;
            CurrentInventoryValue = 0;
            LaunchRawItem = false;
            CalculatedMaxInventoryValue = false;

            for (int i = 0; i < StaticVariables.NumberofPOs; i++)
            {
                MaterialDates newDate = new MaterialDates
                {
                    Number = i + 1,
                    OrderBy = DateTime.MinValue,
                    Estimated = DateTime.MinValue
                };
                newDate.PO.Date = DateTime.MinValue;
                newDate.PO.OrderNumber = "";
                myDates.Add(newDate);
            }

            myError = "";
        }

        public void GetItemInfo()
        {
            using (var conn = new NpgsqlConnection(ExtensionMethod.ConnectionString_ERPSVR1))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT item_number
	                                        ,item_descrip1
                                        FROM item
                                        WHERE item_id = @ItemID";
                    cmd.Parameters.AddWithValue("@ItemID", ItemId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemNumber = reader.GetString(0);
                            ItemDescription = reader.GetString(1);
                        }
                    }
                }
                conn.Close();
            }

            using (var conn = new NpgsqlConnection(ExtensionMethod.ConnectionString_ERPSVR1))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT itemsite_value FROM itemsite
                                            WHERE itemsite_item_id = @ItemID AND itemsite_qtyonhand > 0";
                    cmd.Parameters.AddWithValue("@ItemID", ItemId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CurrentInventoryValue = reader.IsDBNull(0) ? 0 : Convert.ToInt32(reader.GetDouble(0));
                        }
                    }
                }
                conn.Close();
            }
        }

        public bool GetOpenPOs()
        {
            bool HasBlanketPO = false;

            using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP"))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT  poitem_duedate
	                                            ,pohead_number
                                            FROM item
                                            INNER JOIN itemsite ON itemsite_item_id = item_id
                                            INNER JOIN poitem ON poitem_itemsite_id = itemsite_id
                                            INNER JOIN pohead ON pohead_id = poitem_pohead_id
                                            WHERE item.item_id =  @RawMatID AND (poitem_qty_received = 0 OR poitem_duedate >= CURRENT_DATE - 1) AND poitem_duedate > CURRENT_DATE - 2 AND poitem_qty_ordered > 0 AND pohead_status = 'O'
                                            ORDER BY poitem_duedate";
                    cmd.Parameters.AddWithValue("@RawMatID", ItemId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        int counter = 0;
                        while (reader.Read())
                        {
                            if (counter < StaticVariables.NumberofPOs)
                            {
                                HavePO = true;
                                myDates[counter].PO.Date = Convert.ToDateTime(reader.GetDateTime(0));
                                myDates[counter].PO.OrderNumber = Convert.ToString(reader.GetString(1));
                                myDates[counter].PO.CheckForBlanketPO(Convert.ToString(reader.GetString(1)), ItemId);
                                if (myDates[counter].PO.PartBlanketPO)
                                {
                                    HasBlanketPO = true;
                                }
                                counter++;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
                conn.Close();
            }

            return HasBlanketPO;
        }

        public void SetItemStatusFlag()
        {
            myError = "";
            //The Item needs usage otherwise we set the flag to no usage
            if (WeeklyUsage > 0)
            {
                //Danger
                bool Danger_OutOfMaterial = false;
                bool Danger_NeedNewOrUpdatedPO = false;
                bool Danger_NoPO = false;
                bool Danger_OutOfMaterialNeedPO = false;
                bool Warning_NeedNewPO = false;
                bool Warning_UnderMinInventory = Inventory < CalcMinInventory;
                bool Warning_POLate = false;
                bool Warning_InventoryValueHigh = false;
                bool Warning_InventoryFutureHigh = false;
                bool Warning_UsageIssue = false;
                string ToMuchInventoryString = "";
                int NumberOfPOsInSystem = 0;

                //If the user has both history and forecast selected we need to make sure both values are not 0 if they are we need to throw a warning
                if (History > 0 && Forecast)
                {
                    if (MainHistoryValue == 0 || MainForecastValue == 0)
                    {
                        Warning_UsageIssue = true;
                    }
                }

                //if the current inventory value we got from the itemsite is higher than the filled in max value throw a warning
                if (CurrentInventoryValue > MaxInventoryValue)
                {
                    Warning_InventoryValueHigh = true;
                }

                for (int i = 0; i < myDates.Count; i++)
                {
                    //If we Have a PO
                    if (!myDates[i].PO.OrderNumber.Equals(""))
                    {
                        NumberOfPOsInSystem++;

                        //If we will be out of material on PO date throw error
                        if (myDates[i].RunOutOfMaterial)
                        {
                            Danger_OutOfMaterial = true;
                        }
                        else
                        {
                            //Check to see if during any PO date the inventory value will be high
                            if (myDates[i].PO.EstInventoryValue > MaxInventoryValue)
                            {
                                Warning_InventoryFutureHigh = true;
                                if (ToMuchInventoryString.Equals(""))
                                {
                                    ToMuchInventoryString = myDates[i].PO.OrderNumber;
                                }
                            }
                        }

                        if (myDates[i].OrderBy <= DateTime.Today.AddDays(-5) && myDates[i].RunOutOfMaterial)
                        {
                            Danger_NeedNewOrUpdatedPO = true;
                        }
                        if (myDates[i].POLate)
                        {
                            Warning_POLate = true;
                        }
                    }
                    //If we dont have a PO
                    else
                    {
                        if (myDates[i].OrderBy <= DateTime.Today)
                        {
                            if (myDates[i].RunOutOfMaterial)
                            {
                                Danger_OutOfMaterialNeedPO = true;
                            }
                        }
                        if (myDates[i].OrderBy <= DateTime.Today.AddDays(-5))
                        {
                            Warning_NeedNewPO = true;
                        }
                    }

                    if (myDates[i].OrderBy <= DateTime.Today && myDates[i].PO.OrderNumber.Equals(""))
                    {
                        Danger_NoPO = true;
                    }
                }

                //NewItem
                if (NewItem)
                {
                    Flag = MaterialTrackerStatic.MTL_Flag_NewItem;
                    FlagClass = "panel-default";
                }
                //Danger
                else if (Danger_OutOfMaterial || Danger_NeedNewOrUpdatedPO || Danger_NoPO || Danger_OutOfMaterialNeedPO)
                {
                    Flag = MaterialTrackerStatic.MTL_Flag_Danger;
                    FlagClass = "panel-danger";

                    if (Danger_OutOfMaterial)
                    {
                        myError += (myError != "" ? "<br />" : "") + "-Danger: Inventory will reach 0 based on Usage and Current Inventory Level.";
                    }
                    if (Danger_NeedNewOrUpdatedPO)
                    {
                        myError += (myError != "" ? "<br />" : "") + "-Danger: Need to Update or Add a PO in the system to maintain Inventory.";
                    }
                    if (Danger_NoPO)
                    {
                        myError += (myError != "" ? "<br />" : "") + "-Danger: Need new PO in system to cover demand.";
                    }
                    else if (Danger_OutOfMaterialNeedPO)
                    {
                        myError += (myError != "" ? "<br />" : "") + "-Danger: Need new PO in system to cover demand.";
                    }
                }
                //Warning
                else if (Warning_NeedNewPO || Warning_UnderMinInventory || Warning_POLate || Warning_InventoryValueHigh || Warning_InventoryFutureHigh || Warning_UsageIssue)
                {
                    Flag = MaterialTrackerStatic.MTL_Flag_Low;
                    FlagClass = "panel-warning";

                    if (Warning_NeedNewPO)
                    {
                        myError += (myError != "" ? "<br />" : "") + "-Warning: A new PO will need to be added in the next few days to maintain inventory.";
                    }
                    if (Warning_POLate)
                    {
                        myError += (myError != "" ? "<br />" : "") + "-Warning: PO is later then estimated Release Date.";
                    }
                    if (Warning_UnderMinInventory)
                    {
                        myError += (myError != "" ? "<br />" : "") + "-Warning: Inventory is currently less than Minimum Inventory";
                    }
                    if (Warning_InventoryValueHigh)
                    {
                        myError += (myError != "" ? "<br />" : "") + "-Warning: Inventory Value is Currently Higher than Max";
                    }
                    if (Warning_InventoryFutureHigh)
                    {
                        myError += (myError != "" ? "<br />" : "") + "-Warning: Inventory Value will be higher than Max Inventory Value starting on PO# " + ToMuchInventoryString;
                    }
                    if (Warning_UsageIssue)
                    {
                        myError += (myError != "" ? "<br />" : "") + "-Warning: Usage set as Both History and Forecast but one value is 0. Please Update or know that only the non-zero value is being used.";
                    }
                }
                //Normal
                else
                {
                    Flag = MaterialTrackerStatic.MTL_Flag_Normal;
                    FlagClass = "panel-success";
                }
            }
            else
            {
                //NewItem
                if (NewItem)
                {
                    Flag = MaterialTrackerStatic.MTL_Flag_NewItem;
                    FlagClass = "panel-default";
                }
                else
                {
                    Flag = MaterialTrackerStatic.MTL_Flag_NoUsage;
                    FlagClass = "panel-info";
                }
            }

            WarningMessage = myError;
        }

        //Returns the date that the material will run out if it will run out
        public DateTime FindDayMaterialRunsOut()
        {
            double DaysLeft = getWeeksLeft(Inventory, WeeklyUsage) * 7;
            DateTime CheckDate = DateTime.Today.AddDays(DaysLeft);

            for (int i = 0; i < StaticVariables.NumberofPOs; i++)
            {
                if (myDates[i].PO.Date != DateTime.MinValue)
                {
                    if (CheckDate.CompareTo(myDates[i].PO.Date) < 0)
                    {
                        return CheckDate;
                    }
                    else
                    {
                        DaysLeft = getWeeksLeft((Inventory + (ReleaseQty * (i + 1))), WeeklyUsage) * 7;
                        CheckDate = DateTime.Today.AddDays(DaysLeft);
                    }
                }
                else break;
            }

            return DateTime.MinValue;
        }

        public double getWeeksLeft(int _CurrentInventory, int _WeeklyUsage)
        {
            double WeeksLeft = 0;
            //If weekly usage is 0 then we just set the _weeksleft to two years in the future as a temporary measure to fix it.
            if (_WeeklyUsage == 0)
            {
                WeeksLeft = 1004;
            }
            else
            {

                WeeksLeft = (double)_CurrentInventory / (double)_WeeklyUsage;
            }

            return WeeksLeft;
        }

        public void GetReleasesDates()
        {
            double AddDate = 0;

            //need 8 release dates out shown
            for (int i = 0; i < StaticVariables.NumberofPOs; i++)
            {
                //Error Checking to make sure we are not dividing by Zero if the material hasnt been used in a month.
                if (WeeklyUsage == 0)
                {
                    WeeklyUsage = -1;
                    AddDate = -100;
                }

                if (WeeklyUsage != -1)
                {
                    AddDate = (((double)Inventory - (double)CalcMinInventory + ((double)ReleaseQty * i)) / (double)WeeklyUsage) * 7;
                }

                if (AddDate != -100)
                {
                    //We add the days here, but we cant accept orders in on the weekend so we push those orders to monday's
                    System.DateTime Date = System.DateTime.Now;
                    Date = Date.AddDays(AddDate >= 0 && AddDate < 1000000 ? AddDate : 0);
                    if (Date.DayOfWeek == DayOfWeek.Saturday)
                    {
                        Date = Date.AddDays(2);
                    }
                    else if (Date.DayOfWeek == DayOfWeek.Sunday)
                    {
                        Date = Date.AddDays(1);
                    }
                    //all math has been done set our date
                    myDates[i].Estimated = Date;
                    myDates[i].OrderBy = Date.AddDays((LeadTime * 7) * -1);
                }
                else
                {
                    myDates[i].Estimated = DateTime.MinValue;
                    myDates[i].OrderBy = DateTime.MinValue;
                    WeeklyUsage = 0;
                }
            }
        }

        public void LoadSingleItem(int _tblid, int _UserID)
        {
            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"SELECT item_id
	                                    ,release_qty
	                                    ,order_frequency
	                                    ,history_weight
	                                    ,forecast_weight
	                                    ,forecast
	                                    ,history
	                                    ,vend_id
	                                    ,lead_time
	                                    ,updated_at
	                                    ,Id
                                        ,min_inv
                                        ,history_usage
                                        ,forecast_usage
                                        ,user_watch
                                        ,max_inventory_value
                                    FROM material_trackers
                                    WHERE Id = @tblid";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@tblid", _tblid);
                cmd.Connection.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ItemId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                        ReleaseQty = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                        OrderFrequency = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                        HistoryWeight = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                        ForecastWeight = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                        Forecast = reader.IsDBNull(5) ? false : reader.GetBoolean(5);
                        History = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                        VendorID = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);
                        LeadTime = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                        LastUpdated = reader.IsDBNull(9) ? DateTime.MinValue : reader.GetDateTime(9);
                        ListItemID = reader.GetInt32(10);
                        MinInv = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);
                        MainHistoryValue = reader.IsDBNull(12) ? 0 : reader.GetInt32(12);
                        MainForecastValue = reader.IsDBNull(13) ? 0 : reader.GetInt32(13);
                        UserWatch = reader.IsDBNull(14) ? "" : reader.GetString(14);
                        MaxInventoryValue = reader.IsDBNull(15) ? 0 : reader.GetInt32(15);
                        double value = (MainHistoryValue * Convert.ToDouble(HistoryWeight / 100)) + (MainForecastValue * Convert.ToDouble(MainForecastValue / 100));
                        WeeklyUsage = Forecast && History > 0 ? Convert.ToInt32(value) : (Forecast ? MainForecastValue : MainHistoryValue);

                        GetItemInfo();

                        SelectUserWatch = UserWatch.Contains("&" + _UserID + "&");

                        if (ItemNumber.Contains("RES"))
                        {
                            itemType = 5;
                        }
                        else if (ItemNumber.Contains("COM"))
                        {
                            itemType = 8;
                        }
                        else if (ItemNumber.Contains("COL"))
                        {
                            itemType = 6;
                        }
                        else if (ItemNumber.Contains("PKG"))
                        {
                            itemType = 7;
                        }
                    }
                }
                cmd.Connection.Close();
            }
        }

        public void GetVendorName()
        {
            using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP"))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"select vend_name from vendinfo where vend_id = @VendID";
                    cmd.Parameters.AddWithValue("@VendID", VendorID);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Vendor = reader.GetString(0);
                        }
                    }
                }
            }
        }

        public void PORunOutOfMaterialBeforeIn()
        {
            double EstimatedPricePerUnit = Convert.ToDouble(CurrentInventoryValue) / Convert.ToDouble(Inventory);

            for (int i = 0; i < myDates.Count; i++)
            {
                //If has a PO we need to check to make sure if that PO will keep up with material usage
                if (!myDates[i].PO.OrderNumber.Equals(""))
                {
                    //Get Estimated material left on the date of the PO
                    int TotalInventory = Inventory + (i * ReleaseQty);
                    double TotalUsage = ((myDates[i].PO.Date - DateTime.Today).TotalDays / 7) * WeeklyUsage;
                    int MaterialLeft = TotalInventory - Convert.ToInt32(TotalUsage);
                    //Gives us based on PO dates what the estimated Inventory Value will be.
                    myDates[i].PO.EstInventoryValue = (ReleaseQty + MaterialLeft) * EstimatedPricePerUnit;

                    //sets up hover on PO Text
                    myDates[i].PO.EstMaterialLeft = (MaterialLeft > CalcMinInventory ? MaterialLeft.ToString("#,###") + " Est. Remaining Before Order comes in." : "<h8>" + MaterialLeft.ToString("#,###") + " Est. Remaining Before Order comes in." + "</h8>") + "<hr />" +
                          (myDates[i].PO.EstInventoryValue > MaxInventoryValue ? "<h8>Estimated Inventory Value After Release: " + myDates[i].PO.EstInventoryValue.ToString("$#,###") + "</h8>" : "Estimated Inventory Value After Release: " + myDates[i].PO.EstInventoryValue.ToString("$#,###"));

                    myDates[i].RunOutOfMaterial = MaterialLeft <= 0;
                    myDates[i].POLate = myDates[i].PO.Date >= myDates[i].Estimated;
                }
                //No PO and we need to check if we need one in to maintain inventory within leadtime
                else
                {
                    DateTime Test = DateTime.Today.AddDays(LeadTime * -7);

                    if (myDates[i].OrderBy < Test)
                    {
                        double MaterialUsed = LeadTime * WeeklyUsage;

                        if (Inventory > 0)
                        {
                            double MaterialRemaining = MaterialUsed / Inventory;
                            myDates[i].NeedNewPO = MaterialRemaining <= LeadTime;
                        }
                    }

                }
            }
        }

        public int GetWeeklyUsageForHistoryAndForecast()
        {
            if (MainHistoryValue > 0 && MainForecastValue > 0)
            {
                double tmpHistoryWeight = Convert.ToDouble(HistoryWeight) / 100;
                double tmpForecastWeight = Convert.ToDouble(ForecastWeight) / 100;

                double tmpHistoryValue = MainHistoryValue * tmpHistoryWeight;
                double tmpForecastValue = MainForecastValue * tmpForecastWeight;

                WeeklyUsage = Convert.ToInt32(tmpHistoryValue + tmpForecastValue);
            }
            else if (MainForecastValue > 0)
            {
                WeeklyUsage = MainForecastValue;
            }
            else
            {
                WeeklyUsage = MainHistoryValue;
            }

            return WeeklyUsage;
        }

        public void CreateHoverInfoString()
        {
            if (History > 0 && Forecast)
            {
                HoverInfo = (MainHistoryValue == 0 ? "<h8>" : "") + "History: " + History + " Months<br />History Usage: " + MainHistoryValue.ToString("#,###") + "<br />History Weight: " + HistoryWeight + "%" + (MainHistoryValue == 0 ? "</h8>" : "") +
                    "<hr />" + (MainForecastValue == 0 ? "<h8>" : "") + "Forecast Usage: " + MainForecastValue.ToString("#,###") + "<br />Forecast Weight: " + ForecastWeight + "%" + (MainForecastValue == 0 ? "</h8>" : "");
            }
            else if (History > 0)
            {
                HoverInfo = "History: " + History + " Months<br />History Usage: " + MainHistoryValue.ToString("#,###");
            }
            else if (Forecast)
            {
                HoverInfo = "Forecast";
            }
        }

        public void LoadNotes()
        {
            int Index = 1;
            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"SELECT mat_note from material_tracker_notes where mat_tracker_id = @MatID";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@MatID", ListItemID);
                cmd.Connection.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        MatTrackNote newNote = new MatTrackNote
                        {
                            note = reader.IsDBNull(0) ? "" : reader.GetString(0),
                            Index = Index
                        };

                        myNotes.Add(newNote);
                        Index++;
                    }
                }
                cmd.Connection.Close();
            }
        }

        public int GetCurrentInventory(string _itemNumber, bool _Resin)
        {
            //create conenction string and initialize variable to the erpsvr1 
            using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP"))
            {
                //open the connection to the database
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    //parse through the table based on the query string and add it into variable in class
                    cmd.Connection = conn;
                    if (_Resin)
                    {
                        cmd.CommandText = "select nonblend_netable_qty from stone.inv_iteminfo(@ItemNumber, false)";
                        cmd.Parameters.AddWithValue("@ItemNumber", _itemNumber);
                        try
                        {
                            using (var reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    //get inventory from database and set our inventory variable
                                    return reader.IsDBNull(0) ? 0 : Convert.ToInt32(reader.GetDouble(0));
                                }
                            }
                        }
                        catch
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        string ItemNumber = _itemNumber + "%";
                        cmd.CommandText = "SELECT item_number, itemsite_qtyonhand FROM item JOIN itemsite on itemsite_item_id = item_id where item_number ilike @ItemNumber";
                        cmd.Parameters.AddWithValue("@ItemNumber", ItemNumber);
                        int Value = 0;
                        try
                        {
                            using (var reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    //get inventory from database and set our inventory variable
                                    Value += reader.IsDBNull(1) ? 0 : Convert.ToInt32(reader.GetDouble(1));
                                }
                            }
                        }
                        catch
                        {
                            return 0;
                        }
                        return Value;
                    }
                }
                //close the connection
                conn.Close();
            }
            return 0;
        }

        public string UpdateUserWatchString(bool _SelectedUserWatch, int _UserID, int _tblid)
        {
            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"SELECT user_watch
                                    FROM material_trackers
                                    WHERE Id = @tblid";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@tblid", _tblid);
                cmd.Connection.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        UserWatch = reader.IsDBNull(0) ? "" : reader.GetString(0);

                    }
                }
                cmd.Connection.Close();
            }

            if (_SelectedUserWatch)
            {
                //If the box is selected and the string contains the user then we are good to go
                if (UserWatch.Contains("&" + _UserID + "&"))
                {
                    return UserWatch;
                }
                //If the box is selected and the string doesn't contain the user we need to add it
                else
                {
                    UserWatch += "&" + _UserID + "&";
                }
            }
            else
            {
                //if the box is not selected and the string contains it then we need to remove it.
                if (UserWatch.Contains("&" + _UserID + "&"))
                {
                    UserWatch = UserWatch.Replace("&" + _UserID + "&", "");
                }
            }

            return UserWatch;
        }

        //Only used for when an item is saved on edit
        public void GetWeeklyUsage()
        {
            MainHistoryValue = 0;
            MainForecastValue = 0;

            //Get History
            if (History > 0)
            {
                // To get report closer to Xtuples Time Phased Usage
                int DaysLookBack = 31;

                switch (History)
                {
                    case 1:
                        DaysLookBack = 31;
                        break;
                    case 2:
                        DaysLookBack = 59;
                        break;
                    case 3:
                        DaysLookBack = 90;
                        break;
                    default:
                        DaysLookBack = 31;
                        break;
                }

                using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP;Command Timeout=0"))
                {
                    conn.Open();
                    using (var cmd = new NpgsqlCommand())
                    {
                        //Query string is run that grabs last x Months worth of inventory usage and does math to get weekly usage
                        cmd.Connection = conn;
                        cmd.CommandText = @"SELECT sum(invhist_invqty)
                                            FROM invhist
                                            INNER JOIN itemsite ON itemsite_id = invhist_itemsite_id
                                            INNER JOIN item ON item.item_id = itemsite_item_id
                                            WHERE item_id = @ItemID AND invhist_transdate > (CURRENT_DATE - @DaysBack) AND invhist_transdate <= CURRENT_DATE AND invhist_transtype ilike 'ad'";
                        cmd.Parameters.AddWithValue("@ItemID", ItemId);
                        cmd.Parameters.AddWithValue("@DaysBack", DaysLookBack);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                double tmpValue = reader.IsDBNull(0) ? 0 : reader.GetDouble(0) * -1;
                                double tmpdaysback = DaysLookBack / 7;
                                double tmpfinalvalue = tmpValue / tmpdaysback;

                                MainHistoryValue = reader.IsDBNull(0) ? 0 : Convert.ToInt32(tmpfinalvalue);
                            }
                        }
                    }
                    conn.Close();
                }
            }

            //Get Forecast
            if (Forecast)
            {
                //create conenction string and initialize variable to the erpsvr1 
                using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP"))
                {
                    //open the connection to the database
                    conn.Open();
                    using (var cmd = new NpgsqlCommand())
                    {
                        //parse through the table based on the query string and add it into variable in class
                        cmd.Connection = conn;
                        cmd.CommandText = @"SELECT Table_1.RawMaterialNumber
	                                                ,SUM(Table_1.Math)::INT AS TotalMath
                                                FROM (
	                                                SELECT DISTINCT i.item_number AS RawMaterialNumber
		                                                ,pschitem_qty * bomitem_qtyper AS Math
		                                                ,pschitem_scheddate AS ScheduleDate
		                                                ,ia.item_number AS Item
	                                                FROM bomitem
	                                                INNER JOIN item i ON i.item_id = bomitem_item_id
	                                                INNER JOIN stone.job_items ji ON ji.item_id = bomitem_parent_item_id
	                                                INNER JOIN stone.jobs j ON j.id = ji.job_id
	                                                INNER JOIN rev r ON r.rev_id = bomitem_rev_id
	                                                INNER JOIN itemsite ON itemsite_item_id = i.item_id
	                                                INNER JOIN item ia ON bomitem_parent_item_id = ia.item_id
	                                                INNER JOIN itemsite IM ON IM.itemsite_item_id = ia.item_id
	                                                INNER JOIN xtmfg.pschitem ON IM.itemsite_id = pschitem_itemsite_id
	                                                INNER JOIN xtmfg.pschhead ON pschhead_id = pschitem_pschhead_id
	                                                WHERE r.rev_status = 'A' AND bomitem_expires > CURRENT_DATE AND bomitem_effective < CURRENT_DATE AND pschhead_status = 'R' AND pschitem_status != 'X' AND pschitem_scheddate BETWEEN CURRENT_DATE AND CURRENT_DATE + @DaysOut AND pschitem_qty != 0 AND ia.item_active AND i.item_id = @RawItemID AND (IM.itemsite_sold OR ia.item_number ilike 'WIP%')
	                                                ) Table_1
                                                GROUP BY Table_1.RawMaterialNumber
                                                ORDER BY Table_1.RawMaterialNumber";

                        cmd.Parameters.AddWithValue("@RawItemID", ItemId);
                        int DaysForward = LeadTime >= 4 ? LeadTime * 7 : 28;
                        cmd.Parameters.AddWithValue("@DaysOut", DaysForward);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Get how many days the latest forecast is in our system based on leadtime
                                double tmpForecastDateNumber = GetLastItemForecastDate(); //Need to make sure forecast number is correct
                                double tmpForecastNumber = 0;

                                //get inventory from database and set our inventory variable
                                if (tmpForecastDateNumber != 0)
                                {
                                    tmpForecastNumber = Convert.ToDouble(reader.GetInt32(1)) / tmpForecastDateNumber;
                                }

                                if (double.IsNaN(MainForecastValue) || MainForecastValue < 0)
                                {
                                    tmpForecastNumber = 0;
                                }

                                MainForecastValue = Convert.ToInt32(tmpForecastNumber);
                            }
                        }
                    }
                    conn.Close();
                }
            }

            //both
            if (History > 0 && Forecast)
            {
                GetWeeklyUsageForHistoryAndForecast();
            }
            else if (History > 0)
            {
                WeeklyUsage = MainHistoryValue;
            }
            else
            {
                WeeklyUsage = MainForecastValue;
            }

            //Get Inventory
            Inventory = GetCurrentInventory(ItemNumber, ItemNumber.Contains("RES"));

            //Update Weekly Usage into table and updated_at
            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"UPDATE material_trackers set history_usage = @HistoryUsage, forecast_usage = @ForecastUsage, current_inventory = @CurrInv, updated_at = @Now where item_id = @ItemID";
                SqlCommand cmd = new SqlCommand(sql, conn);
                if (MainHistoryValue == 0)
                {
                    cmd.Parameters.AddWithValue("@HistoryUsage", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@HistoryUsage", MainHistoryValue);
                }
                if (MainForecastValue == 0)
                {
                    cmd.Parameters.AddWithValue("@ForecastUsage", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ForecastUsage", MainForecastValue);
                }
                cmd.Parameters.AddWithValue("@CurrInv", Inventory);
                cmd.Parameters.AddWithValue("@Now", DateTime.Now);
                cmd.Parameters.AddWithValue("@ItemID", ItemId);
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
        }

        public double GetLastItemForecastDate()
        {
            //create conenction string and initialize variable to the erpsvr1 
            using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP"))
            {
                //open the connection to the database
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    //parse through the table based on the query string and add it into variable in class
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT pschitem_scheddate AS ScheduleDate
                                        FROM bomitem
                                        INNER JOIN item i ON i.item_id = bomitem_item_id
                                        INNER JOIN stone.job_items ji ON ji.item_id = bomitem_parent_item_id
                                        INNER JOIN stone.jobs j ON j.id = ji.job_id
                                        INNER JOIN rev r ON r.rev_id = bomitem_rev_id
                                        INNER JOIN itemsite ON itemsite_item_id = i.item_id
                                        INNER JOIN item ia ON bomitem_parent_item_id = ia.item_id
                                        INNER JOIN itemsite IM ON IM.itemsite_item_id = ia.item_id
                                        INNER JOIN xtmfg.pschitem ON IM.itemsite_id = pschitem_itemsite_id
                                        INNER JOIN xtmfg.pschhead ON pschhead_id = pschitem_pschhead_id
                                        WHERE r.rev_status = 'A' AND bomitem_expires > CURRENT_DATE AND bomitem_effective < CURRENT_DATE AND pschhead_status = 'R' AND pschitem_status != 'X' AND pschitem_scheddate BETWEEN CURRENT_DATE AND CURRENT_DATE + @DaysOut AND pschitem_qty != 0 AND i.item_id = @RawItemID AND (IM.itemsite_sold OR ia.item_number ilike 'WIP%')
                                        ORDER BY ScheduleDate DESC LIMIT 1;";


                    cmd.Parameters.AddWithValue("@RawItemID", ItemId);
                    int leadtimeDays = LeadTime >= 4 ? LeadTime * 7 : 28;
                    cmd.Parameters.AddWithValue("@DaysOut", leadtimeDays);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            //get inventory from database and set our inventory variable
                            DateTime LastDate = reader.GetDateTime(0);
                            double forecast = ((LastDate - DateTime.Today).TotalDays) / 7;
                            //Get the next highest integer so the forecast number doesn't increase as days go on without a new forecast in the system.
                            if (forecast > 1)
                            {
                                forecast = Math.Floor(forecast);
                            }
                            else
                            {
                                forecast = 1;
                            }
                            return forecast;
                        }
                    }
                }
                conn.Close();
            }
            return 0;
        }

        public void GetIfOnlyLaunchMaterial()
        {
            using (var conn = new NpgsqlConnection(ExtensionMethod.ConnectionString_ERPSVR1))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT *
                                        FROM (
	                                        SELECT count(*) AS cdsa
	                                        FROM charass
	                                        INNER JOIN (
		                                        SELECT parentitem.item_id
		                                        FROM item parentitem
		                                        INNER JOIN bomitem ON bomitem_parent_item_id = parentitem.item_id
		                                        INNER JOIN item rawitem ON rawitem.item_id = bomitem_item_id
		                                        WHERE rawitem.item_id = @ItemID AND parentitem.item_number NOT ilike '%.SMPL' AND parentitem.item_number NOT ilike 'RES%' AND parentitem.item_active AND bomitem_expires > CURRENT_DATE AND bomitem_effective < CURRENT_DATE
		                                        GROUP BY parentitem.item_id
		                                        ) table_1 ON table_1.item_id = charass_target_id
	                                        GROUP BY charass_char_id
	                                        HAVING charass_char_id = 96
	
	                                        UNION ALL
	
	                                        SELECT count(*) AS cdsa
	                                        FROM item parentitem
	                                        INNER JOIN bomitem ON bomitem_parent_item_id = parentitem.item_id
	                                        INNER JOIN item rawitem ON rawitem.item_id = bomitem_item_id
	                                        WHERE rawitem.item_id = @ItemID AND parentitem.item_number NOT ilike '%.SMPL' AND parentitem.item_number NOT ilike 'RES%' AND parentitem.item_active AND bomitem_expires > CURRENT_DATE AND bomitem_effective < CURRENT_DATE
	                                        ) AS table_2";
                    cmd.Parameters.AddWithValue("@ItemID", ItemId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        int count = 0;
                        int LaunchBOMItems = 0;
                        int AllBOMItems = 0;

                        while (reader.Read())
                        {
                            if (count == 0)
                            {
                                LaunchBOMItems = reader.IsDBNull(0) ? 0 : Convert.ToInt32(reader.GetInt64(0));
                                count++;
                            }
                            else
                            {
                                AllBOMItems = reader.IsDBNull(0) ? 0 : Convert.ToInt32(reader.GetInt64(0));
                            }
                        }
                        if (LaunchBOMItems != 0 && AllBOMItems != 0)
                        {
                            LaunchRawItem = LaunchBOMItems == AllBOMItems;
                        }
                    }
                }
                conn.Close();
            }
        }
    }

    [Serializable]
    public class MatTrackData
    {
        public List<MaterialsNode> myMatList = new List<MaterialsNode>();
        public int GoodStanding { get; set; }
        public int Watch { get; set; }
        public int NeedToAddress { get; set; }
        public int NoUsage { get; set; }
        public int Colorants { get; set; }
        public int Components { get; set; }
        public int Packaging { get; set; }
        public int Resin { get; set; }
        public int NewItem { get; set; }
        public int UserWatch { get; set; }
        public string MatSearchString { get; set; }
        public string LowUsageAlertText { get; set; }
        public int BlanketPO { get; set; }

        public MatTrackData()
        {
            GoodStanding = 0;
            Watch = 0;
            NeedToAddress = 0;
            NoUsage = 0;
            Colorants = 0;
            Components = 0;
            Packaging = 0;
            Resin = 0;
            NewItem = 0;
            UserWatch = 0;
            MatSearchString = "";
            BlanketPO = 0;
        }

        public void LoadMaterials(int _Status, int _Type, int _MyWatch, int _UserID, string _SearchString)
        {
            int SearchMatID = 0;
            if (!_SearchString.Equals(""))
            {
                using (var conn = new NpgsqlConnection(ExtensionMethod.ConnectionString_ERPSVR1))
                {
                    //open the connection to the database
                    conn.Open();
                    using (var cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = @"SELECT item_id FROM item where item_number ilike @ItemNumber";
                        cmd.Parameters.AddWithValue("@ItemNumber", _SearchString);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                SearchMatID = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                            }
                        }
                    }
                }
            }

            LowUsageAlertText = "";
            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"SELECT item_id
	                                    ,release_qty
	                                    ,order_frequency
	                                    ,history_weight
	                                    ,forecast_weight
	                                    ,forecast
	                                    ,history
	                                    ,vend_id
	                                    ,lead_time
	                                    ,updated_at
	                                    ,Id
	                                    ,min_inv
	                                    ,history_usage
	                                    ,forecast_usage
	                                    ,low_usage
	                                    ,new_item
                                        ,current_inventory
                                        ,user_watch
                                        ,max_inventory_value
                                        ,mat_type
                                    FROM material_trackers";

                if (SearchMatID != 0)
                {
                    sql += " Where item_id = @ItemID";
                }

                SqlCommand cmd = new SqlCommand(sql, conn);

                if (SearchMatID != 0)
                {
                    cmd.Parameters.AddWithValue("@ItemID", SearchMatID);
                }
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        MaterialsNode addMaterialNode = new MaterialsNode
                        {
                            ItemId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0),
                            ReleaseQty = reader.IsDBNull(1) ? 0 : reader.GetInt32(1),
                            OrderFrequency = reader.IsDBNull(2) ? 0 : reader.GetInt32(2),
                            HistoryWeight = reader.IsDBNull(3) ? 0 : reader.GetInt32(3),
                            ForecastWeight = reader.IsDBNull(4) ? 0 : reader.GetInt32(4),
                            Forecast = reader.IsDBNull(5) ? false : reader.GetBoolean(5),
                            History = reader.IsDBNull(6) ? 0 : reader.GetInt32(6),
                            VendorID = reader.IsDBNull(7) ? 0 : reader.GetInt32(7),
                            LeadTime = reader.IsDBNull(8) ? 0 : reader.GetInt32(8),
                            LastUpdated = reader.IsDBNull(9) ? DateTime.MinValue : reader.GetDateTime(9),
                            ListItemID = reader.GetInt32(10),
                            MinInv = reader.IsDBNull(11) ? 0 : reader.GetInt32(11),
                            MainHistoryValue = reader.IsDBNull(12) ? 0 : reader.GetInt32(12),
                            MainForecastValue = reader.IsDBNull(13) ? 0 : reader.GetInt32(13),
                            LowUsageDate = reader.IsDBNull(14) ? DateTime.MinValue : reader.GetDateTime(14),
                            NewItem = reader.IsDBNull(15) ? false : reader.GetBoolean(15),
                            Inventory = reader.IsDBNull(16) ? 0 : reader.GetInt32(16),
                            UserWatch = reader.IsDBNull(17) ? "" : reader.GetString(17),
                            MaxInventoryValue = reader.IsDBNull(18) ? 0 : reader.GetInt32(18),
                            itemType = reader.IsDBNull(19) ? 0 : reader.GetInt32(19)
                        };

                        //Get Item Number and Description, and inventory value
                        addMaterialNode.GetItemInfo();
                        if (addMaterialNode.OrderFrequency == 0)
                        {
                            addMaterialNode.OrderFrequency = addMaterialNode.LeadTime;
                        }

                        DateTime now = DateTime.Now;
                        bool CheckHasBlanketPO = false;
                        //If the material is not a new item
                        if (!addMaterialNode.NewItem)
                        {
                            addMaterialNode.GetIfOnlyLaunchMaterial();
                            addMaterialNode.Inventory = addMaterialNode.GetCurrentInventory(addMaterialNode.ItemNumber, addMaterialNode.ItemNumber.Contains("RES"));

                            if (addMaterialNode.UserWatch.Contains("&" + _UserID + "&"))
                            {
                                UserWatch++;
                            }

                            if ((addMaterialNode.MainHistoryValue == 0 && addMaterialNode.MainForecastValue == 0))
                            {
                                //addMaterialNode.Inventory = 0;
                                addMaterialNode.WeeksLeft = 0;
                                addMaterialNode.CalcMinInventory = addMaterialNode.MinInv != 0 ? addMaterialNode.MinInv : 0;
                                addMaterialNode.Flag = MaterialTrackerStatic.MTL_Flag_NoUsage;
                                addMaterialNode.FlagClass = "panel-info";
                            }
                            else
                            {
                                //addMaterialNode.Inventory = addMaterialNode.GetCurrentInventory(addMaterialNode.ItemNumber, addMaterialNode.ItemNumber.Contains("RES"));
                                //Get Weekly Usage and then if both history and Forecast then we show how we came up with the number
                                addMaterialNode.WeeklyUsage = addMaterialNode.Forecast && addMaterialNode.History > 0 ? addMaterialNode.GetWeeklyUsageForHistoryAndForecast() : (addMaterialNode.Forecast ? addMaterialNode.MainForecastValue : addMaterialNode.MainHistoryValue);

                                if (addMaterialNode.MaxInventoryValue == 0 && addMaterialNode.Inventory > 0)
                                {
                                    addMaterialNode.MaxInventoryValue = Convert.ToInt32((double)(addMaterialNode.LeadTime * 3) * (double)addMaterialNode.WeeklyUsage * ((double)addMaterialNode.CurrentInventoryValue / (double)addMaterialNode.Inventory));
                                    addMaterialNode.CalculatedMaxInventoryValue = true;
                                }

                                //Get Weeks of Inventory Left based on usage and curent inventory
                                addMaterialNode.WeeksLeft = addMaterialNode.Inventory != 0 && addMaterialNode.WeeklyUsage != 0 ? (double)addMaterialNode.Inventory / (double)addMaterialNode.WeeklyUsage : 0;
                                //either grab calculated
                                addMaterialNode.CalcMinInventory = addMaterialNode.MinInv != 0 ? addMaterialNode.MinInv : (addMaterialNode.OrderFrequency * addMaterialNode.WeeklyUsage);
                                CheckHasBlanketPO = addMaterialNode.GetOpenPOs();
                                if (CheckHasBlanketPO)
                                {
                                    addMaterialNode.Flag = 50;
                                    BlanketPO++;
                                }
                                addMaterialNode.GetReleasesDates();
                                addMaterialNode.PORunOutOfMaterialBeforeIn();
                                addMaterialNode.SetItemStatusFlag();
                            }
                        }
                        else
                        {
                            addMaterialNode.Inventory = 0;
                            addMaterialNode.WeeksLeft = 0;
                            addMaterialNode.CalcMinInventory = 0;
                            addMaterialNode.Flag = MaterialTrackerStatic.MTL_Flag_NewItem;
                            addMaterialNode.FlagClass = "panel-default";
                            NewItem++;
                        }

                        //Checks to see if the Toggles and the Item/Item Status match and if so we add the item into our list for rendering
                        if (AddItemToList(_Status, _Type, addMaterialNode.Flag, addMaterialNode.itemType, addMaterialNode.NewItem, _UserID, addMaterialNode.UserWatch, _MyWatch, CheckHasBlanketPO) || SearchMatID != 0)
                        {
                            //Only Incriment if we are adding to the list
                            if (addMaterialNode.Flag == MaterialTrackerStatic.MTL_Flag_Normal)
                            {
                                GoodStanding++;
                            }
                            else if (addMaterialNode.Flag == MaterialTrackerStatic.MTL_Flag_Low)
                            {
                                Watch++;
                            }
                            else if (addMaterialNode.Flag == MaterialTrackerStatic.MTL_Flag_Danger)
                            {
                                NeedToAddress++;
                            }
                            else if (addMaterialNode.Flag == MaterialTrackerStatic.MTL_Flag_NoUsage)
                            {
                                NoUsage++;
                            }

                            if(addMaterialNode.itemType == 5)
                            {
                                Resin++;
                            }
                            else if(addMaterialNode.itemType == 8)
                            {
                                Components++;
                            }
                            else if (addMaterialNode.itemType == 6)
                            {
                                Colorants++;
                            }
                            else if (addMaterialNode.itemType == 7)
                            {
                                Packaging++;
                            }

                            //If it is not a new item then we need to create hover strings, load notes, and get the vendor info
                            if (!addMaterialNode.NewItem)
                            {
                                addMaterialNode.CreateHoverInfoString();
                                addMaterialNode.LoadNotes();
                                addMaterialNode.GetVendorName();

                                //We only check LowUsageDate every 4 hours
                                if (addMaterialNode.LowUsageDate < now.AddHours(-4) && addMaterialNode.LowUsageDate <= now && addMaterialNode.Flag == MaterialTrackerStatic.MTL_Flag_Danger)
                                {
                                    //if material current inventory is going to go low within the lead time then we need to send an alert to the user.
                                    LowUsageAlertText += "-There is an issue with " + addMaterialNode.ItemNumber + ". Please see the item and resolve issues.<br />";
                                    //Update our table to say we warned the current user about the issue with the material.
                                    UpdateLowUsageTableValue(addMaterialNode.ListItemID);
                                }
                            }
                            myMatList.Add(addMaterialNode);
                        }
                    }
                }
                cmd.Connection.Close();
            }

            myMatList = myMatList.OrderBy(x => x.ItemNumber).ToList();
        }

        public bool AddItemToList(int _Status, int _Type, int _Flag, int _ItemType, bool _NewItem, int _UserID, string _UserWatchList, int _MyWatch, bool _HasBlanketPO)
        {
            bool AddToList = false;

            //If the user selects their watrch list then we need to check to see if any other toggles are selected and then add the item to the list
            if (_MyWatch == 100 && _UserWatchList.Contains("&" + _UserID + "&"))
            {
                if (_Status == 0 && _Type == 0)
                {
                    AddToList = true;
                }
                else
                {
                    if ((_ItemType == _Type && (_Type >= 5 && _Type <= 9)))
                    {
                        if (_Flag == _Status || _Status == 0)
                        {
                            if (!_NewItem)
                            {
                                AddToList = true;
                            }
                        }
                    }
                    else if (_Flag == _Status)
                    {
                        if (_ItemType == _Type || _Type == 0)
                        {
                            AddToList = true;
                        }
                    }
                }
            }
            //If watch is not selected then we just look at the remaining toggles
            else if (_MyWatch != 100 && (_Flag != 40 || _Status == 40))
            {
                if ((_ItemType == _Type && (_Type >= 5 && _Type <= 9)))
                {
                    if (_Flag == _Status || _Status == 0)
                    {
                        if (!_NewItem)
                        {
                            AddToList = true;
                        }
                    }
                }
                else if (_Flag == _Status)
                {
                    if (_ItemType == _Type || _Type == 0)
                    {
                        AddToList = true;
                    }
                }
                //if Status == 1000 then that is just new items
                if (_Status == MaterialTrackerStatic.MTL_Flag_NewItem && _NewItem)
                {
                    if ((_ItemType == _Type && (_Type >= 5 && _Type <= 9)))
                    {
                        AddToList = true;
                    }
                    else if (_Type == 0)
                    {
                        AddToList = true;
                    }
                }
                if ((_Status == 0) && _Type == 0)
                {
                    if (!_NewItem)
                    {
                        AddToList = true;
                    }
                }
                if (_Status == 50 && _HasBlanketPO)
                {
                    AddToList = true;
                }
            }

            return AddToList;
        }

        public void UpdateLowUsageTableValue(int _tblid)
        {
            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"UPDATE material_trackers set low_usage = @Now where Id = @MaterialTrackerID";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@Now", DateTime.Now);
                cmd.Parameters.AddWithValue("@MaterialTrackerID", _tblid);
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
        }
    }

    [Serializable]
    public class ItemInfo
    {
        public int ItemID { get; set; }
        public string ItemNumber { get; set; }

        public ItemInfo()
        {
            ItemID = 0;
            ItemNumber = "";
        }
    }

    //=========================================================================================================
    //==============================        INFO CLASSES                 ======================================
    //=========================================================================================================
    [Serializable]
    public class Info
    {
        public int PageType { get; set; }
        public int Item_ID { get; set; }
        public string ItemNumber { get; set; }
        public string ItemDescription { get; set; }
        public List<LocationNode> myLocations = new List<LocationNode>();

        public List<HistoryUsageNode> myHistoryUsage = new List<HistoryUsageNode>();
        public List<ForecastUsageNode> myForecastUsage = new List<ForecastUsageNode>();
        public List<WhereUsedNode> myWhereUsed = new List<WhereUsedNode>();

        public Info()
        {
            PageType = 0;
            Item_ID = 0;
        }

        public void GetItemInfo()
        {
            using (var conn = new NpgsqlConnection(ExtensionMethod.ConnectionString_ERPSVR1))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "select item_number, item_descrip1 from item where item_id = @ItemID";
                    cmd.Parameters.AddWithValue("@ItemID", Item_ID);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemNumber = reader.GetString(0);
                            ItemDescription = reader.GetString(1);
                        }
                    }
                }
                conn.Close();
            }
        }

        public void GetItemID(int _tblid)
        {
            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"SELECT item_id
                                    FROM material_trackers
                                    WHERE Id = @tblid";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@tblid", _tblid);
                cmd.Connection.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Item_ID = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }
                }
                cmd.Connection.Close();
            }

            GetItemInfo();
        }

        public void LoadLocations()
        {
            using (var conn = new NpgsqlConnection(ExtensionMethod.ConnectionString_ERPSVR1))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT location_rack || '' || location_name as location, itemloc_qty, location_descrip FROM item 
                                            JOIN itemsite on itemsite.itemsite_item_id = item.item_id
                                            JOIN itemloc on itemloc.itemloc_itemsite_id = itemsite.itemsite_id
                                            JOIN location on location.location_id = itemloc_location_id
                                            where item_id = @ItemID
                                            order by location";
                    cmd.Parameters.AddWithValue("@ItemID", Item_ID);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LocationNode newNode = new LocationNode
                            {
                                location_name = reader.IsDBNull(0) ? "" : reader.GetString(0),
                                location_qty = reader.IsDBNull(1) ? 0 : reader.GetDouble(1),
                                location_mutli = reader.IsDBNull(2) ? false : reader.GetString(2).Equals("MultiConsumption")
                            };

                            myLocations.Add(newNode);
                        }
                    }
                }
                conn.Close();
            }
        }

        public void LoadUsage(int _tblid)
        {
            bool Forecast = false;
            int History = 0;
            int DaysLookBack = 0;
            int LeadTime = 0;

            //need to load forecast and history settings
            using (SqlConnection conn = new SqlConnection(ExtensionMethod.Get2016ServerConnectionString()))
            {
                string sql = @"SELECT forecast, history, item_id, lead_time
                                    FROM material_trackers
                                    WHERE Id = @tblid";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@tblid", _tblid);
                cmd.Connection.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Forecast = reader.IsDBNull(0) ? false : reader.GetBoolean(0);
                        History = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                        Item_ID = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                        LeadTime = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                    }
                }
                cmd.Connection.Close();
            }

            switch (History)
            {
                case 1:
                    DaysLookBack = 31;
                    break;
                case 2:
                    DaysLookBack = 59;
                    break;
                case 3:
                    DaysLookBack = 90;
                    break;
                default:
                    DaysLookBack = 31;
                    break;
            }

            //then we need to load history usage and/or forecast usage
            if (History > 0)
            {
                using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP;Command Timeout=0"))
                {
                    conn.Open();
                    using (var cmd = new NpgsqlCommand())
                    {
                        //Query string is run that grabs last x Months worth of inventory usage and does math to get weekly usage
                        cmd.Connection = conn;
                        cmd.CommandText = @"SELECT invhist_invqty, invhist_qoh_before, invhist_qoh_after, invhist_transdate
                                                    FROM invhist
                                                    INNER JOIN itemsite ON itemsite_id = invhist_itemsite_id
                                                    INNER JOIN item ON item.item_id = itemsite_item_id
                                                    WHERE item_id = @ItemID AND invhist_transdate > (CURRENT_DATE - @DaysLookBack) AND invhist_transdate <= CURRENT_DATE AND invhist_transtype ilike 'ad' order by invhist_transdate desc";
                        cmd.Parameters.AddWithValue("@ItemID", Item_ID);
                        cmd.Parameters.AddWithValue("@DaysLookBack", DaysLookBack);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                double tmpqtychange = reader.IsDBNull(0) ? 0 : reader.GetDouble(0);

                                if (myHistoryUsage.Count > 0)
                                {
                                    if (myHistoryUsage[myHistoryUsage.Count - 1].qtychange + tmpqtychange == 0)
                                    {
                                        myHistoryUsage.RemoveAt(myHistoryUsage.Count - 1);
                                    }
                                    else
                                    {
                                        HistoryUsageNode newHist = new HistoryUsageNode
                                        {
                                            qtychange = reader.IsDBNull(0) ? 0 : reader.GetDouble(0),
                                            qtybefore = reader.IsDBNull(1) ? 0 : reader.GetDouble(1),
                                            qtyafter = reader.IsDBNull(2) ? 0 : reader.GetDouble(2),
                                            transactionDate = reader.IsDBNull(3) ? DateTime.MinValue : reader.GetDateTime(3)
                                        };
                                        myHistoryUsage.Add(newHist);
                                    }
                                }
                                else
                                {
                                    HistoryUsageNode newHist = new HistoryUsageNode
                                    {
                                        qtychange = reader.IsDBNull(0) ? 0 : reader.GetDouble(0),
                                        qtybefore = reader.IsDBNull(1) ? 0 : reader.GetDouble(1),
                                        qtyafter = reader.IsDBNull(2) ? 0 : reader.GetDouble(2),
                                        transactionDate = reader.IsDBNull(3) ? DateTime.MinValue : reader.GetDateTime(3)
                                    };
                                    myHistoryUsage.Add(newHist);
                                }
                            }
                        }
                    }
                    conn.Close();
                }
            }

            if (Forecast)
            {
                using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP;Command Timeout=0"))
                {
                    conn.Open();
                    using (var cmd = new NpgsqlCommand())
                    {
                        //Query string is run that grabs last x Months worth of inventory usage and does math to get weekly usage
                        cmd.Connection = conn;
                        cmd.CommandText = @"SELECT DISTINCT pschitem_qty * bomitem_qtyper AS Math
	                                                        ,pschitem_scheddate AS ScheduleDate
	                                                        ,ia.item_number AS Item
	                                                        ,pschitem_qty
                                                        FROM bomitem
                                                        INNER JOIN item i ON i.item_id = bomitem_item_id
                                                        INNER JOIN stone.job_items ji ON ji.item_id = bomitem_parent_item_id
                                                        INNER JOIN stone.jobs j ON j.id = ji.job_id
                                                        INNER JOIN rev r ON r.rev_id = bomitem_rev_id
                                                        INNER JOIN itemsite ON itemsite_item_id = i.item_id
                                                        INNER JOIN item ia ON bomitem_parent_item_id = ia.item_id
                                                        INNER JOIN itemsite IM ON IM.itemsite_item_id = ia.item_id
                                                        INNER JOIN xtmfg.pschitem ON IM.itemsite_id = pschitem_itemsite_id
                                                        INNER JOIN xtmfg.pschhead ON pschhead_id = pschitem_pschhead_id
                                                        WHERE 	r.rev_status = 'A' AND 
		                                                        bomitem_expires > CURRENT_DATE AND bomitem_effective < CURRENT_DATE AND 
		                                                        pschhead_status = 'R' AND 
		                                                        pschitem_status != 'X' AND pschitem_scheddate BETWEEN CURRENT_DATE AND CURRENT_DATE + @DaysForward AND 
		                                                        pschitem_qty != 0 AND 
		                                                        ia.item_active AND 
		                                                        i.item_id = @ItemID AND 
		                                                        (IM.itemsite_sold OR ia.item_number ilike 'WIP%')
		                                                        order by pschitem_scheddate, ia.item_number";
                        cmd.Parameters.AddWithValue("@ItemID", Item_ID);
                        //Need to look forward at least 28 days into the future
                        int DaysForward = LeadTime >= 4 ? LeadTime * 7 : 28;
                        cmd.Parameters.AddWithValue("@DaysForward", DaysForward);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ForecastUsageNode newFore = new ForecastUsageNode
                                {
                                    qtyneeded = reader.IsDBNull(0) ? 0 : reader.GetDouble(0),
                                    scheduleddate = reader.IsDBNull(1) ? DateTime.MinValue : reader.GetDateTime(1),
                                    FGName = reader.IsDBNull(2) ? "" : reader.GetString(2),
                                    FGRequired = reader.IsDBNull(3) ? 0 : reader.GetInt32(3)
                                };

                                myForecastUsage.Add(newFore);
                            }
                        }
                    }
                    conn.Close();
                }
            }
        }

        public void LoadWhereUsed()
        {
            using (var conn = new NpgsqlConnection(ExtensionMethod.ConnectionString_ERPSVR1))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT parentitem.item_number
	                                            ,parentitem.item_descrip1
	                                            ,bomitem_qtyper
                                            FROM item rawitem
                                            INNER JOIN bomitem ON bomitem_item_id = rawitem.item_id
                                            INNER JOIN item parentitem ON parentitem.item_id = bomitem_parent_item_id
                                            WHERE rawitem.item_id = @ItemID AND parentitem.item_number NOT ilike '%.SMPL' AND parentitem.item_number NOT ilike 'RES%' AND parentitem.item_active AND bomitem_expires > CURRENT_DATE AND bomitem_effective < CURRENT_DATE
                                            GROUP BY parentitem.item_number
	                                            ,parentitem.item_descrip1
	                                            ,bomitem_qtyper
                                            ORDER BY parentitem.item_number";
                    cmd.Parameters.AddWithValue("@ItemID", Item_ID);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            WhereUsedNode newNode = new WhereUsedNode
                            {
                                ItemNumber = reader.IsDBNull(0) ? "" : reader.GetString(0),
                                ItemDescrip1 = reader.IsDBNull(1) ? "" : reader.GetString(1),
                                QtyPer = reader.IsDBNull(2) ? 0 : reader.GetDouble(2)
                            };

                            myWhereUsed.Add(newNode);
                        }
                    }
                }
                conn.Close();
            }
        }
    }

    [Serializable]
    public class WhereUsedNode
    {
        public string ItemNumber { get; set; }
        public string ItemDescrip1 { get; set; }
        public double QtyPer { get; set; }

        public WhereUsedNode()
        {
            ItemNumber = "";
            ItemDescrip1 = "";
            QtyPer = 0;
        }
    }

    [Serializable]
    public class LocationNode
    {
        public string location_name { get; set; }
        public double location_qty { get; set; }
        public bool location_mutli { get; set; }

        public LocationNode()
        {
            location_name = "";
            location_qty = 0;
            location_mutli = false;
        }
    }

    [Serializable]
    public class HistoryUsageNode
    {
        public double qtychange { get; set; }
        public double qtybefore { get; set; }
        public double qtyafter { get; set; }
        public DateTime transactionDate { get; set; }

        public HistoryUsageNode()
        {
            qtychange = 0;
            qtybefore = 0;
            qtyafter = 0;
            transactionDate = DateTime.MinValue;
        }
    }

    [Serializable]
    public class ForecastUsageNode
    {
        public double qtyneeded { get; set; }
        public DateTime scheduleddate { get; set; }
        public string FGName { get; set; }
        public int FGRequired { get; set; }

        public ForecastUsageNode()
        {
            qtyneeded = 0;
            scheduleddate = DateTime.MinValue;
            FGName = "";
            FGRequired = 0;
        }
    }
}