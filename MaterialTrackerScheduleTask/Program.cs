﻿using System;
using System.Collections.Generic;
using Npgsql;
using System.Data.SqlClient;

namespace MaterialTrackerScheduleTask
{
    class Program
    {
        static int Main(string[] args)
        {
            MatTrackData newMatList = new MatTrackData();

            newMatList.LoadMaterials(0, 0);

            return 0;
        }
    }

    public static class ExpansionMethods
    {
        public static string Get2016ServerConnectionString()
        {
            string HostName = System.Net.Dns.GetHostName();
            if (HostName.ToLower().Equals("sptasksvr1"))
            {
                return "Data Source=SPMSSQL16Prod;Initial Catalog=SharePointData;Persist Security Info=True;User ID=readwrite;Password=246stone!";
            }
            else
            {
                return "Data Source=SPMSSQL16test;Initial Catalog=SharePointDataDev;Persist Security Info=True;User ID=admin;Password=stone246";
            }
        }
    }

    public class MaterialsNode
    {
        public int ListItemID { get; set; }

        //Item information
        public string ItemNumber { get; set; }
        public int ItemId { get; set; }
        public string ItemDescription { get; set; }
        public int VendorID { get; set; }

        //Material Tracker Information
        public int ReleaseQty { get; set; }
        public int LeadTime { get; set; }
        public int CalcMinInventory { get; set; }
        public DateTime LastUpdated { get; set; }
        public int WeeklyUsage { get; set; }
        public int Inventory { get; set; }
        public double WeeksLeft { get; set; }
        public int MinInv { get; set; }
        public int OrderFrequency { get; set; }
        public DateTime LowUsageDate { get; set; }
        public bool NewItem { get; set; }

        //History and/or Forecast
        public int History { get; set; }
        public bool Forecast { get; set; }
        public int HistoryWeight { get; set; }
        public int ForecastWeight { get; set; }
        public int DaysLookForward { get; set; }
        public int DaysLookPast { get; set; }
        public int MainHistoryValue { get; set; }
        public int MainForecastValue { get; set; }

        public MaterialsNode()
        {
            ItemNumber = "";
            ItemId = -1;
            VendorID = -1;
            ReleaseQty = -1;
            LeadTime = -1;
            CalcMinInventory = -1;
            ItemDescription = "";

            WeeklyUsage = 0;
            Inventory = -1;
            WeeksLeft = -1;
            HistoryWeight = 50;
            ForecastWeight = 50;
            OrderFrequency = 0;
        }

        public void GetItemInfo()
        {
            using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP"))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "select item_number, item_descrip1 from item where item_id = @ItemID";
                    cmd.Parameters.AddWithValue("@ItemID", ItemId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemNumber = reader.GetString(0);
                            ItemDescription = reader.GetString(1);
                        }
                    }
                }
                conn.Close();
            }
        }

        public double getWeeksLeft(int _CurrentInventory, int _WeeklyUsage)
        {
            double WeeksLeft = 0;
            //If weekly usage is 0 then we just set the _weeksleft to two years in the future as a temporary measure to fix it.
            if (_WeeklyUsage == 0)
            {
                WeeksLeft = 1004;
            }
            else
            {

                WeeksLeft = (double)_CurrentInventory / (double)_WeeklyUsage;
            }

            return WeeksLeft;
        }

        public void GetWeeklyUsage()
        {
            MainHistoryValue = 0;
            MainForecastValue = 0;

            //Get History
            if (History > 0)
            {
                // To get report closer to Xtuples Time Phased Usage
                int DaysLookBack = 31;

                switch (History)
                {
                    case 1:
                        DaysLookBack = 31;
                        break;
                    case 2:
                        DaysLookBack = 59;
                        break;
                    case 3:
                        DaysLookBack = 90;
                        break;
                    default:
                        DaysLookBack = 31;
                        break;
                }

                using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP;Command Timeout=0"))
                {
                    conn.Open();
                    using (var cmd = new NpgsqlCommand())
                    {
                        //Query string is run that grabs last x Months worth of inventory usage and does math to get weekly usage
                        cmd.Connection = conn;
                        cmd.CommandText = @"SELECT sum(invhist_invqty)
                                            FROM invhist
                                            INNER JOIN itemsite ON itemsite_id = invhist_itemsite_id
                                            INNER JOIN item ON item.item_id = itemsite_item_id
                                            WHERE item_id = @ItemID AND invhist_transdate > (CURRENT_DATE - @DaysBack) AND invhist_transdate <= CURRENT_DATE AND invhist_transtype ilike 'ad'";
                        cmd.Parameters.AddWithValue("@ItemID", ItemId);
                        cmd.Parameters.AddWithValue("@DaysBack", DaysLookBack);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                double tmpValue = reader.IsDBNull(0) ? 0 : reader.GetDouble(0) * -1;
                                double tmpdaysback = DaysLookBack / 7;
                                double tmpfinalvalue = tmpValue / tmpdaysback;

                                MainHistoryValue = reader.IsDBNull(0) ? 0 : Convert.ToInt32(tmpfinalvalue);
                            }
                        }
                    }
                    conn.Close();
                }
            }

            //Get Forecast
            if (Forecast)
            {
                //create conenction string and initialize variable to the erpsvr1 
                using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP"))
                {
                    //open the connection to the database
                    conn.Open();
                    using (var cmd = new NpgsqlCommand())
                    {
                        //parse through the table based on the query string and add it into variable in class
                        cmd.Connection = conn;
                        cmd.CommandText = @"SELECT Table_1.RawMaterialNumber
	                                                ,SUM(Table_1.Math)::INT AS TotalMath
                                                FROM (
	                                                SELECT DISTINCT i.item_number AS RawMaterialNumber
		                                                ,pschitem_qty * bomitem_qtyper AS Math
		                                                ,pschitem_scheddate AS ScheduleDate
		                                                ,ia.item_number AS Item
	                                                FROM bomitem
	                                                INNER JOIN item i ON i.item_id = bomitem_item_id
	                                                INNER JOIN stone.job_items ji ON ji.item_id = bomitem_parent_item_id
	                                                INNER JOIN stone.jobs j ON j.id = ji.job_id
	                                                INNER JOIN rev r ON r.rev_id = bomitem_rev_id
	                                                INNER JOIN itemsite ON itemsite_item_id = i.item_id
	                                                INNER JOIN item ia ON bomitem_parent_item_id = ia.item_id
	                                                INNER JOIN itemsite IM ON IM.itemsite_item_id = ia.item_id
	                                                INNER JOIN xtmfg.pschitem ON IM.itemsite_id = pschitem_itemsite_id
	                                                INNER JOIN xtmfg.pschhead ON pschhead_id = pschitem_pschhead_id
	                                                WHERE r.rev_status = 'A' AND bomitem_expires > CURRENT_DATE AND bomitem_effective < CURRENT_DATE AND pschhead_status = 'R' AND pschitem_status != 'X' AND pschitem_scheddate BETWEEN CURRENT_DATE AND CURRENT_DATE + @DaysOut AND pschitem_qty != 0 AND ia.item_active AND i.item_id = @RawItemID AND (IM.itemsite_sold OR ia.item_number ilike 'WIP%')
	                                                ) Table_1
                                                GROUP BY Table_1.RawMaterialNumber
                                                ORDER BY Table_1.RawMaterialNumber";

                        cmd.Parameters.AddWithValue("@RawItemID", ItemId);
                        int DaysForward = LeadTime >= 4 ? LeadTime * 7 : 28;
                        cmd.Parameters.AddWithValue("@DaysOut", DaysForward);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Get how many days the latest forecast is in our system based on leadtime
                                double tmpForecastDateNumber = GetLastItemForecastDate(); //Need to make sure forecast number is correct
                                double tmpForecastNumber = 0;

                                //get inventory from database and set our inventory variable
                                if (tmpForecastDateNumber != 0)
                                {
                                    tmpForecastNumber = Convert.ToDouble(reader.GetInt32(1)) / tmpForecastDateNumber;
                                }

                                if (double.IsNaN(MainForecastValue) || MainForecastValue < 0)
                                {
                                    tmpForecastNumber = 0;
                                }

                                MainForecastValue = Convert.ToInt32(tmpForecastNumber);
                            }
                        }
                    }
                    conn.Close();
                }
            }

            //both
            if (History > 0 && Forecast)
            {
                GetWeeklyUsageForHistoryAndForecast();
            }
            else if (History > 0)
            {
                WeeklyUsage = MainHistoryValue;
            }
            else
            {
                WeeklyUsage = MainForecastValue;
            }

            //Get Inventory
            Inventory = GetCurrentInventory(ItemNumber, ItemNumber.Contains("RES"));

            //Update Weekly Usage into table and updated_at
            using (SqlConnection conn = new SqlConnection(ExpansionMethods.Get2016ServerConnectionString()))
            {
                string sql = @"UPDATE material_trackers set history_usage = @HistoryUsage, forecast_usage = @ForecastUsage, current_inventory = @CurrInv, updated_at = @Now where Id = @MaterialTrackerID";
                SqlCommand cmd = new SqlCommand(sql, conn);
                if (MainHistoryValue == 0)
                {
                    cmd.Parameters.AddWithValue("@HistoryUsage", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@HistoryUsage", MainHistoryValue);
                }
                if (MainForecastValue == 0)
                {
                    cmd.Parameters.AddWithValue("@ForecastUsage", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ForecastUsage", MainForecastValue);
                }
                cmd.Parameters.AddWithValue("@CurrInv", Inventory);
                cmd.Parameters.AddWithValue("@Now", DateTime.Now);
                cmd.Parameters.AddWithValue("@MaterialTrackerID", ListItemID);
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
        }

        public double GetLastItemForecastDate()
        {
            //create conenction string and initialize variable to the erpsvr1 
            using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP"))
            {
                //open the connection to the database
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    //parse through the table based on the query string and add it into variable in class
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT pschitem_scheddate AS ScheduleDate
                                        FROM bomitem
                                        INNER JOIN item i ON i.item_id = bomitem_item_id
                                        INNER JOIN stone.job_items ji ON ji.item_id = bomitem_parent_item_id
                                        INNER JOIN stone.jobs j ON j.id = ji.job_id
                                        INNER JOIN rev r ON r.rev_id = bomitem_rev_id
                                        INNER JOIN itemsite ON itemsite_item_id = i.item_id
                                        INNER JOIN item ia ON bomitem_parent_item_id = ia.item_id
                                        INNER JOIN itemsite IM ON IM.itemsite_item_id = ia.item_id
                                        INNER JOIN xtmfg.pschitem ON IM.itemsite_id = pschitem_itemsite_id
                                        INNER JOIN xtmfg.pschhead ON pschhead_id = pschitem_pschhead_id
                                        WHERE r.rev_status = 'A' AND bomitem_expires > CURRENT_DATE AND bomitem_effective < CURRENT_DATE AND pschhead_status = 'R' AND pschitem_status != 'X' AND pschitem_scheddate BETWEEN CURRENT_DATE AND CURRENT_DATE + @DaysOut AND pschitem_qty != 0 AND i.item_id = @RawItemID AND (IM.itemsite_sold OR ia.item_number ilike 'WIP%')
                                        ORDER BY ScheduleDate DESC LIMIT 1;";

                    cmd.Parameters.AddWithValue("@RawItemID", ItemId);
                    int leadtimeDays = LeadTime >= 4 ? LeadTime * 7 : 28;
                    cmd.Parameters.AddWithValue("@DaysOut", leadtimeDays);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            //get inventory from database and set our inventory variable
                            DateTime LastDate = reader.GetDateTime(0);
                            double forecast = ((LastDate - DateTime.Today).TotalDays) / 7;
                            //Get the next highest integer so the forecast number doesn't increase as days go on without a new forecast in the system.
                            if (forecast > 1)
                            {
                                forecast = Math.Floor(forecast);
                            }
                            else
                            {
                                forecast = 1;
                            }
                            return forecast;
                        }
                    }
                }
                conn.Close();
            }
            return 0;
        }

        public int GetWeeklyUsageForHistoryAndForecast()
        {
            double tmpHistoryWeight = Convert.ToDouble(HistoryWeight) / 100;
            double tmpForecastWeight = Convert.ToDouble(ForecastWeight) / 100;

            double tmpHistoryValue = MainHistoryValue * tmpHistoryWeight;
            double tmpForecastValue = MainForecastValue * tmpForecastWeight;

            WeeklyUsage = Convert.ToInt32(tmpHistoryValue + tmpForecastValue);

            return WeeklyUsage;
        }

        public int GetCurrentInventory(string _itemNumber, bool _Resin)
        {
            //create conenction string and initialize variable to the erpsvr1 
            using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP"))
            {
                //open the connection to the database
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    //parse through the table based on the query string and add it into variable in class
                    cmd.Connection = conn;
                    if (_Resin)
                    {
                        cmd.CommandText = "select nonblend_netable_qty from stone.inv_iteminfo(@ItemNumber, false)";
                        cmd.Parameters.AddWithValue("@ItemNumber", _itemNumber);
                        try
                        {
                            using (var reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    //get inventory from database and set our inventory variable
                                    return reader.IsDBNull(0) ? 0 : Convert.ToInt32(reader.GetDouble(0));
                                }
                            }
                        }
                        catch
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        string ItemNumber = _itemNumber + "%";
                        cmd.CommandText = "SELECT item_number, itemsite_qtyonhand FROM item JOIN itemsite on itemsite_item_id = item_id where item_number ilike @ItemNumber";
                        cmd.Parameters.AddWithValue("@ItemNumber", ItemNumber);
                        int Value = 0;
                        try
                        {
                            using (var reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    //get inventory from database and set our inventory variable
                                    Value += reader.IsDBNull(1) ? 0 : Convert.ToInt32(reader.GetDouble(1));
                                }
                            }
                        }
                        catch
                        {
                            return 0;
                        }
                        return Value;
                    }
                }
                //close the connection
                conn.Close();
            }
            return 0;
        }
    }

    public class AllRawMatsInfo
    {
        public int item_id { get; set; }
        public string item_number { get; set; }
        public bool in_tracker { get; set; }
    }

    public class MatTrackData
    {
        public List<MaterialsNode> myMatList = new List<MaterialsNode>();

        public void CheckNewItems()
        {
            List<AllRawMatsInfo> AllRaws = new List<AllRawMatsInfo>();

            //Load in all raws
            using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP"))
            {
                //open the connection to the database
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    //parse through the table based on the query string and add it into variable in class
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT item_id, item_number
                                        FROM item
                                        WHERE 	(item_number ilike 'COM%' OR 
		                                        item_number ilike 'RES%' OR 
		                                        item_number ilike 'COL%' OR 
		                                        item_number ilike 'PKG%') AND 
		                                        item_active";
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AllRawMatsInfo newMat = new AllRawMatsInfo();
                            newMat.item_id = reader.GetInt32(0);
                            newMat.item_number = reader.IsDBNull(1) ? "" : reader.GetString(1);
                            newMat.in_tracker = false;
                            AllRaws.Add(newMat);
                        }
                    }
                }
            }

            //Load in all the items from our sql database
            using (SqlConnection conn = new SqlConnection(ExpansionMethods.Get2016ServerConnectionString()))
            {
                string sql = @"select item_id from material_trackers";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        foreach (AllRawMatsInfo AllRawNode in AllRaws)
                        {
                            if (AllRawNode.item_id == reader.GetInt32(0))
                            {
                                AllRawNode.in_tracker = true;
                                break;
                            }
                        }
                    }
                }
                cmd.Connection.Close();
            }

            //cycle through all our loaded raws from xtuple
            foreach (AllRawMatsInfo AllRawNode in AllRaws)
            {
                bool ItemInBOM = false;
                bool HasProductionServiceOrLaunchJob = false;

                //Check to see if the item is in the bom of a FG/WIP
                using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP"))
                {
                    //open the connection to the database
                    conn.Open();
                    using (var cmd = new NpgsqlCommand())
                    {
                        //parse through the table based on the query string and add it into variable in class
                        cmd.Connection = conn;
                        cmd.CommandText = @"SELECT count(*)
                                        FROM bomitem
                                        INNER JOIN item ON bomitem_parent_item_id = item.item_id
                                        WHERE bomitem_item_id = @ItemID AND bomitem_expires > CURRENT_DATE AND bomitem_effective < CURRENT_DATE AND item.item_active";
                        cmd.Parameters.AddWithValue("@ItemID", AllRawNode.item_id);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.GetInt32(0) > 0)
                                {
                                    ItemInBOM = true;
                                }
                            }
                        }
                    }
                }

                //If it is in the bom of an item, we need to check to see if 
                if(ItemInBOM)
                {
                    //Check to see if the item is in the bom of a FG/WIP
                    using (var conn = new NpgsqlConnection("Host=erpsvr1;Username=reporting;Password=password;Database=STONE_ERP"))
                    {
                        //open the connection to the database
                        conn.Open();
                        using (var cmd = new NpgsqlCommand())
                        {
                            //parse through the table based on the query string and add it into variable in class
                            cmd.Connection = conn;
                            cmd.CommandText = @"SELECT jobs.job_status_id FROM item 
                                                    JOIN bomitem on bomitem_parent_item_id = item.item_id
                                                    JOIN stone.job_items on job_items.item_id = item.item_id
                                                    JOIN stone.jobs on jobs.id = job_items.job_id
                                                    where bomitem_item_id = @ItemID and item.item_active";
                            cmd.Parameters.AddWithValue("@ItemID", AllRawNode.item_id);
                            using (var reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    if (reader.GetInt32(0) < 7)
                                    {
                                        HasProductionServiceOrLaunchJob = true;
                                    }
                                }
                            }
                        }
                    }
                }



                //Not In Tracker and is in a bom we need to add it
                if (!AllRawNode.in_tracker && ItemInBOM && HasProductionServiceOrLaunchJob)
                {
                    using (SqlConnection conn = new SqlConnection(ExpansionMethods.Get2016ServerConnectionString()))
                    {
                        string sql = @"INSERT INTO material_trackers (item_id, release_qty, order_frequency, vend_id, lead_time, updated_at, min_inv, history_weight, forecast_weight, forecast, history, low_usage, new_item, active, mat_type) 
                                    VALUES (@ItemID, @ReleaseQty, @Order, @Vend, @Lead, @Now, @MinInv, @HisWeight, @ForWeight, @Forecast, @History, @LowUsage, @NewItem, @ItemActive, @MatTypeID)";
                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.Parameters.AddWithValue("@ItemID", AllRawNode.item_id);
                        cmd.Parameters.AddWithValue("@ReleaseQty", 0);
                        cmd.Parameters.AddWithValue("@Order", 0);
                        cmd.Parameters.AddWithValue("@Vend", 0);
                        cmd.Parameters.AddWithValue("@Lead", 0);
                        cmd.Parameters.AddWithValue("@Now", DateTime.Now.AddHours(-2));//Make the updated at -2 so when we go back to the main page it updates all the values
                        cmd.Parameters.AddWithValue("@MinInv", 0);
                        cmd.Parameters.AddWithValue("@HisWeight", 50);
                        cmd.Parameters.AddWithValue("@ForWeight", 50);
                        cmd.Parameters.AddWithValue("@Forecast", true);
                        cmd.Parameters.AddWithValue("@History", 3);
                        cmd.Parameters.AddWithValue("@LowUsage", DateTime.Now.AddDays(-1));
                        cmd.Parameters.AddWithValue("@NewItem", true);
                        cmd.Parameters.AddWithValue("@ItemActive", true);

                        if (AllRawNode.item_number.Contains("RES"))
                        {
                            cmd.Parameters.AddWithValue("@MatTypeID", 5);
                        }
                        else if (AllRawNode.item_number.Contains("COM"))
                        {
                            cmd.Parameters.AddWithValue("@MatTypeID", 8);
                        }
                        else if (AllRawNode.item_number.Contains("COL"))
                        {
                            cmd.Parameters.AddWithValue("@MatTypeID", 6);
                        }
                        else if (AllRawNode.item_number.Contains("PKG"))
                        {
                            cmd.Parameters.AddWithValue("@MatTypeID", 7);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@MatTypeID", DBNull.Value);
                        }


                        cmd.Connection.Open();
                        cmd.ExecuteNonQuery();
                        cmd.Connection.Close();
                        Console.WriteLine("Added Item ID: " + AllRawNode.item_id);
                    }
                }
                //if it is in both then make sure that it is active in our list
                else if (AllRawNode.in_tracker && ItemInBOM && HasProductionServiceOrLaunchJob)
                {
                    using (SqlConnection conn = new SqlConnection(ExpansionMethods.Get2016ServerConnectionString()))
                    {
                        string sql = @"UPDATE material_trackers SET active = @ItemActive, mat_type = @MatTypeID where item_id = @ItemID";
                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.Parameters.AddWithValue("@ItemID", AllRawNode.item_id);
                        cmd.Parameters.AddWithValue("@ItemActive", true);

                        if (AllRawNode.item_number.Contains("RES"))
                        {
                            cmd.Parameters.AddWithValue("@MatTypeID", 5);
                        }
                        else if (AllRawNode.item_number.Contains("COM"))
                        {
                            cmd.Parameters.AddWithValue("@MatTypeID", 8);
                        }
                        else if (AllRawNode.item_number.Contains("COL"))
                        {
                            cmd.Parameters.AddWithValue("@MatTypeID", 6);
                        }
                        else if (AllRawNode.item_number.Contains("PKG"))
                        {
                            cmd.Parameters.AddWithValue("@MatTypeID", 7);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@MatTypeID", DBNull.Value);
                        }

                        cmd.Connection.Open();
                        cmd.ExecuteNonQuery();
                        cmd.Connection.Close();
                        Console.WriteLine("Make sure Item Active is true for Item ID: " + AllRawNode.item_id);
                    }
                }
                //is in tracker but not in a bom we need to deactivate the part
                else if (AllRawNode.in_tracker && (!ItemInBOM || !HasProductionServiceOrLaunchJob))
                {
                    using (SqlConnection conn = new SqlConnection(ExpansionMethods.Get2016ServerConnectionString()))
                    {
                        string sql = @"UPDATE material_trackers SET active = @ItemActive where item_id = @ItemID";
                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.Parameters.AddWithValue("@ItemID", AllRawNode.item_id);
                        cmd.Parameters.AddWithValue("@ItemActive", false);
                        cmd.Connection.Open();
                        cmd.ExecuteNonQuery();
                        cmd.Connection.Close();
                        Console.WriteLine("Removed Item ID: " + AllRawNode.item_id);
                    }
                }
            }
            Console.WriteLine("Checked for new Items Complete");
        }

        public void LoadMaterials(int _Status, int _Type)
        {
            CheckNewItems();
            int TotalTrack = 0;
            int Count = 0;
            using (SqlConnection conn = new SqlConnection(ExpansionMethods.Get2016ServerConnectionString()))
            {
                string sql = @"select count(*) from material_trackers";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        TotalTrack = reader.GetInt32(0);
                    }
                }
                cmd.Connection.Close();
            }

            using (SqlConnection conn = new SqlConnection(ExpansionMethods.Get2016ServerConnectionString()))
            {
                string sql = @"SELECT item_id
	                                    ,release_qty
	                                    ,order_frequency
	                                    ,history_weight
	                                    ,forecast_weight
	                                    ,forecast
	                                    ,history
	                                    ,vend_id
	                                    ,lead_time
	                                    ,updated_at
	                                    ,Id
	                                    ,min_inv
	                                    ,history_usage
	                                    ,forecast_usage
	                                    ,low_usage
	                                    ,new_item
                                        ,current_inventory
                                    FROM material_trackers";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        MaterialsNode addMaterialNode = new MaterialsNode
                        {
                            ItemId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0),
                            ReleaseQty = reader.IsDBNull(1) ? 0 : reader.GetInt32(1),
                            OrderFrequency = reader.IsDBNull(2) ? 0 : reader.GetInt32(2),
                            HistoryWeight = reader.IsDBNull(3) ? 0 : reader.GetInt32(3),
                            ForecastWeight = reader.IsDBNull(4) ? 0 : reader.GetInt32(4),
                            Forecast = reader.IsDBNull(5) ? false : reader.GetBoolean(5),
                            History = reader.IsDBNull(6) ? 0 : reader.GetInt32(6),
                            VendorID = reader.IsDBNull(7) ? 0 : reader.GetInt32(7),
                            LeadTime = reader.IsDBNull(8) ? 0 : reader.GetInt32(8),
                            LastUpdated = reader.IsDBNull(9) ? DateTime.MinValue : reader.GetDateTime(9),
                            ListItemID = reader.GetInt32(10),
                            MinInv = reader.IsDBNull(11) ? 0 : reader.GetInt32(11),
                            MainHistoryValue = reader.IsDBNull(12) ? 0 : reader.GetInt32(12),
                            MainForecastValue = reader.IsDBNull(13) ? 0 : reader.GetInt32(13),
                            LowUsageDate = reader.IsDBNull(14) ? DateTime.MinValue : reader.GetDateTime(14),
                            NewItem = reader.IsDBNull(15) ? false : reader.GetBoolean(15),
                            Inventory = reader.IsDBNull(16) ? 0 : reader.GetInt32(16)
                        };
                        //Get Item Number and Description
                        addMaterialNode.GetItemInfo();
                        Count++;

                        DateTime now = DateTime.Now;
                        if (!addMaterialNode.NewItem)
                        {
                            addMaterialNode.GetWeeklyUsage();
                        }
                        //Alway write out it being updated
                        Console.WriteLine(addMaterialNode.ItemNumber + " has been updated - " + Count + "/" + TotalTrack);
                    }
                }
                cmd.Connection.Close();
            }
        }
    }
}
